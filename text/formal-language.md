# Formal Language

This file describes notes on a possible formal language the can be used to parse 
stylized facts.

--- 

* Should describe all possible/supported SFs
* Should be able to parse written SFs into machine understandable Hypotheses

> **Idea**: **Expressions** consist of statements which describe one property/function of
> timeseries'.

* Following elements
    * **expression**: Whole SF -> boolean
    * **LTL**: Time based **expression** -> boolean
    * **conditional**: If, except, ... -> boolean
    * **statement**: Atomic expression (dist(x) has heavytail) -> boolean
    * **object**: (Generated data) Thing that can be evaluated (dist, function,
      variable) -> timeseries
    * **variable**: existing data -> timeseries
    * **quantifier**: $\forall, \exists$ -> boolean

## Data (generation)

* **object**: (Generated data) Thing that can be evaluated (dist, function, variable) ->
  timeseries
* **object** := var
* **object** := AC(**object**, Time)
* **object** := CC(**object**, **object**, Time_lag)
* **object** := distribution(**object**)
* **object** := timeseries(**object**)
* **object** := function(**object**)
* **object** := X(**object**), X $\in$ {cumulated, mean, kurtosis, ...}

---

## SF

* **SF** := (**conditional**)* **expression**

## Expression

* **expression** := **statement**
* **expression** := **LTL**
* **expression** := **quantifier** **variable** ( **expression** )
* **expression** := *not* **expression**
* **expression** := **expression** [*And*|*Or*|*?*] **expression**

---

* **conditional** := *if* **boolean_expression**:
* (**conditional** := **expression** *except* **var_assignment**)

---

* **LTL** := **Expression**
* **LTL** := (*Always*|*Eventually*|*Next*|*Never*) **Expression**
* **LTL** := **Expression** *Until* **Expression**
* **LTL** := **LTL** (*preceeds*|*follows*) **LTL**

---

* **statement** := **object** [**strength**] (like|is) **descriptor**
* **statement** := **object** has [**strength**] **property**
* **statement** := **var** is **strength**
* **statement** := not **statement**?

---

* **descriptor** := non-Gaussion, x-tailed, powerlaw, motonically (non-)de-/increasing,
  exact term, u-shape, decaying/declining/loss/falling/growing/stagnates
* **property** := skewness, amplitude, memory

---

* **sensitivity** := **quantifier** x in [range]: y has similar effect
* **sensitivity** := x shows long term persistence

---

* **strength** := (strong|medium|small) [positive|negative]
* **strength** := [in]significant

---

* **var** := (corrected|absolute|accumulated|squared|-2*|total number of| percentage|...)
  **var**
* **var** := Name

---

## Examples

if $!\delta_t$ is small: $AC(x)$ is small

target variable: AC(x)

**conditional**(*IF*, **statement**(*NOT*, **statement**(**object**, *LIKE*, **descriptor
**)), **statement**(**object**, *LIKE*, **descriptor**))

The growth rate of daily deaths from COVID-19 fell rapidly everywhere within the first 30
days after each region reached 25 cumulative deaths.

target variable: GrowthRate(deaths)

AFTER expression1, expression1
AFTER cumulative(deaths) is >= 25, growthrate(deaths) is falling rapidly

LTL(**statement**(**object**(cumulated(**var**)) < 25), *UNTIL*, **statement**(**object**(
growthrate(**var**)) is **descriptor**(rapidly **descriptor**(falling)))

After this first period of rapid decline, the growth rate of daily deaths in all regions
has hovered around zero or slightly below.

### Examples TODO

* expression(if)
* drawdowns(x) > upward_movements(x)
* AC(volatility) positive
* larger $!\delta_t$ correlates to $dist(x) more like normal
* corrected(x) shows heavytails
* if !dist(x) like normal and variance(x) not inf: dist(x) like powerlaw$(2<$tailindex$<
  5)$
* (f(time_lag) = AC(x)) like decaying powerlaw($\beta \in [0.2, 0.4])
* CC(Volatility(x), y) negative
* ​CC(x, volatility) positive
* (f(x)=y) shows (U shaped curve)
* CC(x, y) positive
* CC(x, y) unclear/small
* x "unpredictable" and no large AA(x,ANY)
* dist(x) fat_tailed; AA(volatility(x)) high, long_memory(AA)
* AA(squared(x)) high and AA(absolute(x)) high even at high-order lags and decay slowly
* CC(x, y) high, persistent(x)
* variance(x) > variance(y)
* is small: AC(x)AC(x) is small

### Predicates

#### Timeseries descriptors

AC(var1, time)
CC(var1, var2, time_lag)
long memory, persisent

#### Distribution descriptors

dist(x) like [descriptor]

[descriptor] := non-Gaussion, fat-tailed, powerlaw, ...

dist(x) has high [property]

[property] := skewness, amplitude, ...

[periods and wavelengths] (are in range, decrease with, ...)

#### Function desriptors

f is (approximately) [descriptor]

[descriptor] := motonically (non-)de-/increasing, xy correlated, [any term], u-shape
