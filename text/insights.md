# Fragen

## Wie validieren?

1. Face Validation (plots)
2. Stylized facts

## Welche relevanten formen von stylized Facts gibt es? Wie "stark müssen diese gelten"?

immer gzgl. Output
"Denklager", Problem: muss zur Forschungsfrage passen

Was ist ein Stylized Fact?
VWL: qualitative Muster in bestimmtem Zeitraum
quantitativ: z.B. Standardabweichung, correlation coefficients, ... mit realen Daten. RQ:
wie realistisch soll das Modell sein. SF12 Okun's law
Agent-based models für Forecast
Calibrierung mit Kriging
Fakt muss nicht 100% der Zeit gültig sein

SF2: Exponentielle verteilung
SF3: Correlations "unemployment ist antizyklisch zu GDP", "Investment steigt, kurz bevor
GDP abnimmt",
SF4: Correlations
SF12: Correlation "negativ correliert"

Siehe stylized-facts.md für Beispiele und Charakterisierung

---

## Spezifikation von stylized facts?

In Literatur meist nur wörtlich definiert. Im Finanzsektor oft in Abhängigkeit von
Businesscycles (Prozyklisches Verhalten, Antizyklisches Verhalten,
Versetztzyklisch). Dadurch ggf. durch cross- und autocorrelation zwischen
(bandpass-gefilterter) Zeitreihen spezifizierbar.

---

## Kalibrierung? Validierung?

unser Ziel:

1. Plausibilität vom Modell beim Modellaufbau
   Hypothesen explizit formulieren (z.B. Schwankung soll persistent sein), wie bringe ich
   meine Theorien ins Modell
   GDP, umployment, inflation als wichtigste Größen
   Frage nur nach zyklisch oder antizyklisch (cross-correlation)
   Mehrere stylized facts: gewichten?

Parametrisierung/Fitting über Optimierer -> Human in the loop
mehrere hundert Model building steps

Verifikation: Summe der Größen muss x ergeben

What-if analysis: (Achtung Philipps Bias)

* z.B. Geldsystem geändert (Erwartung dass mehr Stabilität)
* wie kann man das System verändern um ähnliche/bessere Ergebnisse zu erhalten

nicht zyklisch: Schock und Wirtschaftszusammenbruch, Aktienmärkte gehen immer hoch
Interbank markt: Zinsrate soll immer bei 1 liegen

## Gütermarkt-Modell

Denkweise Beispiel:

* was heißt fully employed?
* Strategy change complex enough?

### Parameter screening:

1. gammy_y, gammy_p
   1.1 inventory_target
2. zeta_h, zeta_f

### First Modell building steps

1. firmen
2. angebot & nachfrage
3. firm strategy

### Outputs

* wie lange soll so ein Fluktuationszyklus sein? In wieviel Prozent der Perioden passt es?
* +/- 5 Prozent Richtwert Schwankung

## Qualitative

?
makro- vs mikroökonomisch

---

## Wie wird in der Literatur mit nicht immer gültigen Facts umgegangen?

1. in Zielfunktion mit einkodieren (Siehe Mitra2018)
2. Falls model "overconstrained" -> adaptiv nur nach Lösung suchen, die Teilmenge der
   constraints weights ändern. (partial constraint satisfaction (Siehe Zanker2010)
   in Zanker2010:

---

## Mittels welcher Methoden werden die durch stylized facts festgelegten Constraints erzwungen?

In Literatur meist *face validation* und *model tweaking*

## Sprache für Stylized Facts

Sehr hilfreich!
Frage: existieren so viele Klassen von SF?
