# Stylized facts

This paper contains definitions, categories and examples of stylized facts (SF). It also 
contains operators that are used to describe these SFs.

## Introduction and definition

"interesting, sometimes counterintuitive, patterns in empirical data."

### Characterised by the following 5 facts

![stylized facts characteristics png](images/stylized-facts.png)

---

### Difference stylized fact vs scientific law

![Stylized facts vs scientific laws png](images/sf_vs_sl.png)

*Huoy et al 2015*

(in "Stylized Facts as an Instrument for Literature Review and Cumulative
Information Systems Research")

---

"Establishing the 'stylized facts' associated with a set of time series is
widely considered a crucial step in macroeconomic research (see e.g. Blanchard
and Fischer, 1989 chapter 1" *Harvey et al 1993*"

"The website About.Economics (http://economics.about.com) defines stylized facts
as: ‘observations that have been made in so many contexts that they are widely
understood to be empirical truths, to which theories must fit.’ The definition
goes on to state: ‘[u]sed especially in macroeconomic theory’." *Helfat et al
2007*

---

## Categories

### Types of Stylized Facts

| Category                                              | Examples                                                                                    | Methods |
| ----------------------------------------------------- | ------------------------------------------------------------------------------------------- | ------- |
| Autocorrelations                                      | "linear autocorrelations of asset returns often insignificant"                              | ...     |
| Cross-correlations                                    | "positive cross-correlation between volatility and volume"                                  | ...     |
| Shape of Distribution                                 | "non-Gaussian, fat tailed, large drawdowns and small upward movements, powerlaw, amplitude" | ...     |
| Distribution moments (Skewness, Kurtosis)             | ""                                                                                          | ...     |
| Timescale/Frequency based                             | "With larger $\Delta t$...", "At any $\Delta t$..."                                         | ...     |
| Monotonically (non-)de-/increasing (value/derivative) | "the share of economic activity in agriculture monotonically decreases"                     | ...     |
| Difference                                            | "when compared to other contries..."                                                        | ...     |
| Waves, Seasons, periods                               | "the seasonality of the foodmarket", "the periods and wavelengths of ... decrease with ..." | ...     |
| Other time seriers shapes                             | "inverted U-shape", "demographic transition"                                                | ...     |
| Diversification                                       | "Economic development requires diversification", "poor countries less diversified"          | ...     |
| Absence of ...                                        | "Absence of autocorrelation except for small time scales"                                   | ...     |
| X causes Y                                            | "are typically caused by ..."                                                               | ...     |
| Sensitivity                                           | "long term persistence", "similar effect"                                                   | ...     |
| timeseries memory                                     | "have long memory"                                                                          | ...     |

AC, CC, Shape, Memory, As function of, Moments, Quantifier, Comparison, Relation, Conditional, LTL

### Operators

AC, CC, Lags, Decaying, Persistent, Stationary, Fat tail, Volatility Clustering, Memory, Conditional, Powerlaw, "Falling Wave", Normal Dist, Variance, For all, Filtered by Volatility Clustering, As function of, Spacial Scope, Related, Within timeinterval after event after event, Addition

Strength

* Always/Never
* Almost Always/Never
* Decay/Decline/Loss/Falling
* Growth/Stagnation
* Slow/Fast
* Positive/Negative
* Since/Until
* For all/Exist
* High/Low, Major/Minor effect
* (In-)significant
* Except
* Not
* After correcting/filtering for (e.g. volatility clustering)
* Spatial Scope (for every region, ...)
* Time (after ...)

### Observation

Square, Absolute, time-scale-dependent, Distribution, "Diff measures", Volatility of variables, Difference across regions, (Lag), Growth rate, rate, highest rate, cross regional

* Total number
* Cumulative number
* Fraction/Share/Percentage of
* Agents that are female/under 18/over 80/...

---

## Examples

| Paper      | Example                                                                                                                                              | SF Category                         | Formula                                                                                   |
| ---------- | ---------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------- | ----------------------------------------------------------------------------------------- |
| Cont2000   | linear autocorrelations of asset returns often insignificant except for small time scales (~20 Min)                                                  | Autocorr., time-scale based         | if $!\delta_t$ is small: $AC(x)$ is small                                                 |
|            | distribution of returns displays power law with $2 <$ tail index $< 5$. This excludes stable laws with infinite variance and the normal distribution | Shape(powerlaw), Conditional        | if !dist(x) like normal and variance(x) not inf: dist(x) like powerlaw$(2<$tailindex$<5)$ |
|            | large drawdowns in stock prices and stock index values but not equally large upward movements                                                        | Shape (falling wave)                | drawdowns(x) > upward_movements(x)                                                        |
|            | With larger time scale $\Delta t$, the asset distribution looks more and more like a normal distribution                                             | time-scale based                    | larger $!\delta_t$ correlates to $dist(x) more like normal                                |
|            | Returns show high variability at any $\Delta t$                                                                                                      | ?                                   | variability(x) is high                                                                    |
|            | Diff. measures of volatility show positive autocorrelation over serveral days. (High volatility events cluster in time)                              | autocorr.                           | AC(volatility) positive                                                                   |
|            | Heavy tails for residual time series after correcting for volatility clustering                                                                      | shape/moment (heavy tail), Filtered | corrected(x) shows heavytails                                                             |
|            | AC of absolute returns decays as function of time lag as powerlaw with $\beta \in [0.2, 0.4]$                                                        | Shape(powerlaw), ?                  | (f(time_lag) = AC(x)) like decaying powerlaw($\beta \in [0.2, 0.4])                       |
|            | Most measures of volatility of asset are negatively correlated with returns of asset                                                                 | crosscorr.                          | CC(Volatility(x), y) negative                                                             |
|            | trading volume is correlated with measures of volatility                                                                                             | crosscorr.                          | CC(x, volatility) positive                                                                |
|            | Coarse grained measures of volatility predict fine-scale volatility better than the other way around                                                 | ?                                   | ?                                                                                         |
| Rodrik2006 | Economic development requires diversification, not specialization, inverted for high levels                                                          | shape, diversification              | (f(x)=y) shows (U shaped curve)                                                           |
|            | Rapidly growing countries have large manufacturing sectors                                                                                           | crosscorr.                          | CC(x, y) positive                                                                         |
|            | Growth accelerations are associated with structural changes in the direction of manufacturing                                                        | crosscorr.                          | CC(x, y) unclear/small                                                                    |
|            | Specialization patterns are not pinned down by factor endowments                                                                                     | crosscorr.                          | CC(x, y) positive                                                                         |
|            | Countries that promote exports of more "sophisticated" goods grow faster                                                                             | crosscorr.                          | CC(x,y) positive                                                                          |
|            | There is “unconditional” convergence at the level of individual products                                                                             | ?                                   | ?                                                                                         |
|            | Some specialization patterns are more conducive to others in promoting industrial upgrading                                                          | ?                                   | some CC greater than other CC                                                             |
| Sacks2012  | relationship between well-being and income shows no sign of flattening at higher levels of income                                                    | shape (no flattening)               | f(x)=y does not flatten                                                                   |
|            | Although the 25 countries represent many different cultures and languages, the relationship between income and well-being is quantitatively similar  | sensitivity/crosscorrelation?       | CC(v1, v2) similar over v3-values                                                         |
|            | Economists tend to argue that well-being varies with long-term income prospects, rather than year-to-year income fluctuations.                       | crosscorr., weighing                | CC(x1, y1) greater than CC(x2, y2)                                                        |
| Hommes2002 | asset prices are persistent, have unit root, are thus (close to) nonstationary (unit root := characteristic eq. has root 1)                          | regression                          | ?                                                                                         |
|            | asset returns are fairly unpredictable, and typically have little or no autocorrelations                                                             | autocorr, ?                         | x "unpredictable" and no large AA(x,ANY)                                                  |
|            | asset returns have fat tails and exhibit volatility clustering and long memory.                                                                      | shape, timeseries memory            | dist(x) fat_tailed; AA(volatility(x)) high, long_memory(AA)                               |
|            | Autocorrelations of squared returns and absolute returns are significantly positive, even at high-order lags, and decay slowly                       | autocorr, sensitivity               | AA(squared(x)) high and AA(absolute(x)) high even at high-order lags and decay slowly     |
|            | Trading volume is persistent and there is positive cross-correlation between volatility and volume.                                                  | crosscorr                           | CC(x, y) high, persistent(x)                                                              |
| wikipedia  | "the variance of production exceeds the variance of sales"                                                                                           | moment(variance)                    | variance(x) > variance(y)                                                                 |

### Harvey et al; 1993; detrending, stylized facts and the buisness cycle

Coarse grained measures of volatility predict fine-scale volatility better than the other
way around

### Brandouy et al; 2012; A re-examination of the “zero is enough” hypothesis in the emergence of financial stylized facts

1. for the return time series
    1. absence of autocorrelation except for very small intraday time scales
    2. volatility clustering,
    3. autocorrelation of volumes and
    4. cross correlation volume volatility
2. for the empirical distrFinally, there is no evidence of satiation. Just as we saw when
   making
   cross-national comparison, the estimated relationship between well-being and
   income shows no sign of flattening at higher levels of income time scale increases

They conjecture that unstable markets are more desta- bilizing for traders since
the latter rely more on the crowd which generates unstable dynamics

### Cont et al; 2000; Empirical properties of asset returns

1. ! **Absence of autocorrelations**: linear autocorrelations of asset returns
   often insignificant except for small time scales (~20 Min)
2. **Heavy tails**: distribution of returns displays power law with $2 <$ tail
   index $< 5$. This excludes stable laws with infinite variance and the normal
   distribution
3. **Gain/loss asymmetry**: large drawdowns in stock prices and stock index
   values but not equally large upward movements
4. ! **Aggregational Gaussianity**: With larger time scale $\Delta t$, the asset
   distribution looks more and more like a normal distribution
5. ! **Intermittency**: Returns show high variability at any $\Delta t$
6. **Volatility clustering**: Diff. measures of volatility show positive
   autocorrelation over serveral days. (High volatility events cluster in time)
7. **Conditional heavy tails**: Heavy tails for residual time series after
   correcting for volatility clustering
8. **Slow decay of autocoFinally, there is no evidence of satiation. Just as we saw when
   making
   cross-national comparison, the estimated relationship between well-being and
   income shows no sign of flattening at higher levels of incomerrelation in absolute
   returns**: AC of absolute returns
   decays as function of time lag as powerlaw with $\beta \in [0.2, 0.4]$
9. **Leverage effect**: Most measures of volatility of asset are negatively
   correlated with returns of asset
10. **Volume/volatility correlation**: trading volume is correlated with
    measures of volatility
11. ? **Asymmetry in time scales**: Coarse grained measures of volatility predict
    fine-scale volatility better than the other way around

- Qualitative ones: 1?, 4?, 5?Coarse grained measures of volatility predict
  fine-scale volatility better than the other way around..
- Time-based ones: 4, 5

[the-oi-hartman-abel-effect]{https://medium.com/@bajajashu97/the-oi-hartman-abel-effect-279136f95522}

The Oi Hartman Effect (Oi, 1961; Hartman, 1972; Abel, 1983) highlights the facts that
firms can expand to exploit good outcomes and contract to insure against bad outcomes,
making them potentially risk loving. That is, if agents can flexibly expand to benefit
from good customers and, quickly contract during bad outcomes, they may benefit from
increased uncertainty. Such an effect, however, is believed to be strong only in the
medium to long run.

### Rodrik2006 draft: INDUSTRIAL DEVELOPMENT: STYLIZED FACTS AND POLICIES

1. Economic development requires diversification, not specialization Poor
   countries produce a relatively narrow range of goods, while richer countries
   are engaged in a broad range of economic activities; Discovered that as incomes
   increase, economies become less concentrated and more diversified; Then at high levels:
   specialization again -> U shaped curve
2. Rapidly growing countries have large manufacturing sectors
3. Growth accelerations are associated with structural changes in the direction
   of manufacturing
4. Specialization patterns are not pinned down by factor endowments
5. Countries that promote exports of more "sophisticated" goods grow faster
6. There is “unconditional” convergence at the level of individual products
7. Some specialization patterns are more conducive to others in promoting
   industrial upgrading

### Sacks et al 2012: The new stylized facts about income and subjective well-being

- qualitative (except 4.?)

1. lines all slope upward: in each of the twenty-five countries we see a strong
   relationship whereby richer people on average report higher well-being.
2. the lines are roughly parallel. Although these countries represent many
   different cultures and languages, the relationship between income and well-being
   is quantitatively similar
3. Finally, there is no evidence of satiation. Just as we saw when making
   cross-national comparison, the estimated relationship between well-being and
   income shows no sign of flattening at higher levels of income
4. ! Economists tend to argue that well-being varies with long-term income
   prospects, rather than year-to-year income fluctuations.

### Dosi et al; 1994; Process of Economic Developm

- About growth patterns of country economies

1. Economies have grown over the past two centuries probably faster than during
   any previous period in recorded history.
2. They have grown at different and variable rates (sometimes negative for
   particular periods and particular countries — Argentina, a few less developed
   countries, USSR, etc. — or for many countries for particular periods — i.e. deep
   recessions).
3. The long-term patterns for the whole set of countries show an increasing
   differentiation, highlighted by a secular increase in the variance in per capita
   income.
4. ? Catching up with forging ahead has been relatively rare (Britain overtaking
   Holland in the 18th century; the USA, Germany and others overtaking Britain in
   the late 19th and 20th centuries; Japan overtaking almost everyone in the late
   20th century). Progress in catching up has been more widespread (western—central
   Europe in the 19th century, Scandinavia and Italy in the 20th; East Asian
   countries in the late 20th century; the EEC catching up with the USA during the
   1960s and 1970s).
5. ? Falling behind has been a rather frequent phenomenon, too (many less
   developed countries in the 1970s and 1980s; a few countries falling behind after
   a considerable 'spurt' of catching up—compare the 1950s with the 1980s in Latin
   America and Eastern Europe; Britain experiencing a long- term relative decline).
6. One can hardly identify, in general, persistent features of national growth
   patterns just conditional on initial performances (e.g. 'all laggard countries
   will tend to grow faster' or 'those that have grown faster will grow faster also
   in the future'). Closer inspection of particular economies or groups of them
   does appear to show long-term persistence (e.g. Japan or, conversely, Britain)
   but the causes of the phenomenon are plausibly country-specific rather than a
   common feature of the world economy.

### Hommes et al; 2002; Modeling the stylized facts in finance through simple nonlinear adaptive system

An important goal of agent-based modeling of financial markets is to explain
important observed stylized facts such as

1. asset prices are persistent and have, or are close to having, a unit root and
   are thus (close to) nonstationary (unit root := characteristic equation of diff. eq.
   has root (solution) 1)
2. asset returns are fairly unpredictable, and typically have little or no
   autocorrelations
3. asset returns have fat tails and exhibit volatility clustering and long
   memory. Autocorrelations of squared returns and absolute returns are
   significantly positive, even at high-order lags, and decay slowly
4. Trading volume is persistent and there is positive cross-correlation between
   volatility and volume.

### Hinloopen et al 2008; Empirical relevance of the Hillman condition for revealed comparative advantage: 10 stylized facts

> [Hillman] diagrammatically develops a necessary and sufficient condition for the
> correspondence between the Balassa index and pre-trade relative prices in
> cross-country sector comparisons, the so-called Hillman condition. [...] He awnsers: if
> two countries, $j=1,2$ , are observed to be exporting a particular
> good i in the world market, does $\beta_1 > \beta_2$ imply that, with respect to
> a common numéraire good, good i was relatively less expensive in country 1’s
> autarkic equilibrium than it was in country 2’s autarkic equilibrium?”

1. Violations of the Hillman condition occur in 0 – 0.2 percent of all cases;
   these violations represent 0 – 3.4 percent of total trade
2. The higher is the degree of sector aggregation, the higher is the probability
   that the Hillman condition is violated
3. The value of trade represented by all cases violating the Hillman condition
   is hardly affected by the level of sector aggregation.
4. The Hillman condition is an effective screening device for identifying
   observations of revealed comparative advantage that are based on erroneously
   classified trade flows
5. The Hillman condition suffers from a masking effect in that mild violations
   remain undetected if grotesque violations are present.
6. Concerning violations of the Hillman condition over time two periods can be
   distinguished: (i) 1970 – 1984, during which violations occur relatively
   frequent and represent a substantial fraction of total trade, and (ii) 1985 –
   1997, during which violations hardly ever occur and represent an insignificant
   fraction of total trade.
7. The correlation between sectors violating the Hillman condition across levels
   of sector aggregation is asymmetric; violations at lower levels of sector
   aggregation are likely to occur at higher levels as well, while violations at
   higher levels of sector aggregation need not occur at lower levels.
8. At all levels of aggregation, violations of the Hillman condition occur
   almost exclusively in primary product sectors and to some extent in
   natural-resource intensive sectors.
9. The correlation between countries hosting sectors that violate the Hillman
   condition across levels of sector aggregation is asymmetric; violations at lower
   levels of sector aggregation are likely to occur at higher levels as well, while
   violations at higher levels of sector aggregation need not occur at lower
   levels.
10. At all levels of sector aggregation, violations of the Hillman condition
    occur foremost for observations involving countries in Africa (including the
    Middle East), and, to a lesser extend, involving countries in Latin America, the
    Caribbean, and Eastern Europe.

### Easterly et al 2001 What have we learned from a decade of empirical research on growth? It's Not Factor Accumulation: Stylized Facts and Growth Models

1. The “residual” (total factor productivity, tfp) rather than factor
   accumulation accounts for most of the income and growth differences across
   countries
2. Income diverges over the long run
3. Factor accumulation is persistent while growth is not, and the growth path
   of countries exhibits remarkable variation
4. Economic activity is highly concentrated, with all factors of production
   flowing to the richest areas
5. National policies are closely associated with
   long‐run economic growth rates

### Treiber et al 2010 Three-phase traffic theory and two-phase models with a fundamental diagram in the light of empirical stylized facts

1. Congestion patterns on real (non-circular) freeways are typically caused by bottlenecks
   in combination with a perturbation in the traffic flow
2. The bottleneck may be caused by various reasons such as ...
3. The congestion pattern is either localized with a constant width of the order of 1 km,
   or it is spatially extended with a time-dependent extension.
4. The downstream front of congested traffic is either fixed at the bottleneck, or it
   moves upstream with the characteristic speed c
5. The upstream front of spatially extended congestion patterns has no characteristic
   speed.
6. Most extended traffic patterns show some “internal structure” propagating upstream
   approximately at the same characteristic speed c
7. The periods and wavelengths of internal structures in congested traffic states tend to
   decrease as the severity of congestion increases.
8. For bottlenecks of moderate strength, the amplitude of the internal structures tends to
   increase while propagating upstream.
9. Light or very strong bottlenecks may cause extended traffic patterns, which appear
   homogeneous (uniform in space)

## Examples from Demography

### Child Marriage and Faith Affiliation in Sub-Saharan Africa: Stylized Facts and Heterogeneity

https://doi.org/10.1080/15570274.2015.1075752

* Child marriage has been declining over time, but only slowly.
* The likelihood of marrying early and the number of years of child marriage do differ
  according to faith affiliation.

### Family structure and children’s educational outcomes: Blended families, stylized facts, and descriptive regressions

https://doi.org/10.1353/dem.2004.0031

* Simple correlations between family structure and children’s outcomes
* vs descriptive regressions that control for variables such as mother’s education and
  family income

### Women Left Behind? Poverty and Headship in Africa

https://doi.org/10.1007/s13524-017-0561-7

* Female-headed households tend to be poorer.
* Poverty has been falling in the aggregate since the 1990s.

### MORTALITY, MIGRATION, AND RURAL TRANSFORMATION IN SUB-SAHARAN AFRICA'S URBAN TRANSITION

https://doi.org/10.1017/dem.2016.29

* Urban populations in countries across the region have been growing at historically
  unprecedented rates since the 1960s.
* The urban share of the region's population (i.e. level of urbanization) has grown
  persistently, even during periods of sustained economic stagnation ... in the 1980s and
  1990s (urbanization without growth).

### Agriculture in Africa – Telling myths from facts: A synthesis

https://doi.org/10.1016/j.foodpol.2017.02.002

* 60–80 percent of work in African agriculture is done by women.
* One third of the world’s food is lost post-harvest.
* Land markets play a minor role in African development.
* Seasonality in African food markets is fading.
* Incomes among African farmers are under-diversified.

### Land pressures, the evolution of farming systems, and development strategies in Africa: A synthesis

https://doi.org/10.1016/j.foodpol.2014.05.014

* Africa has surplus land, but it is concentrated within relatively few countries.
* Despite aggregate land abundance, many African farmers have limited potential for land
  expansion and are experiencing declining farm sizes.
* Africa’s rural populations are highly clustered, in both land abundant and land
  constrained settings.
* Africa’s rural population will continue to grow for the next 40 years.

### The Changing Structure of Africa’s Economies

https://doi.org/10.1093/wber/lhw070

* As countries grow, the share of economic activity in agriculture monotonically
  decreases, and the share in services monotonically increases. The share of activity in
  manufacturing appears to follow an inverted U-shape; it increases during low stages of
  development as capital is accumulated and then decreases for high stages of development
  where higher incomes drive demand for services, and labor costs make manufacturing
  difficult.
* First, when the patterns of employment in Africa are compared to the patterns observed
  in other regions across levels of development, the pattern among our sample follows that
  seen in other regions for agriculture and services, that is, the agricultural employment
  share is decreasing in income while the services employment share is increasing in
  income.
* Second, when the levels of employment shares are compared to the levels observed in
  other countries, the levels of employment shares in agriculture and services approximate
  the levels observed in other countries at similar levels of income.
* Third, all of this holds for industry and manufacturing in the eight low-income African
  countries.
* Fourth, in Botswana, Mauritius, and South Africa, the patterns in industry are similar
  but the levels differ, and, in the case of manufacturing, the relationship between
  income and employment shares follows more of an upward sloping line than an inverted
  U-shape.
* Fifth, Africa is still, by far, one of the poorest regions of the world. And finally,
  structural change continues to remain a potent source of labor productivity growth in
  much of SSA.

### Vanishing Third World Emigrants?

https://www.nber.org/papers/w14785

* Emmigration from third world countries has been declining except for Africa.

### Chapter 3 - Demography and Aging

https://doi.org/10.1016/B978-0-12-380880-6.00003-4

* Demographic transition: from high fertility and mortality rates to low fertility and
  mortality rates, and population decrease.
* Epidemiologic transition: shift in predominant causes of death from infectious to
  degenerative diseases.
* See also "sylized patterns" https://link.springer.com/article/10.1007/s00148-016-0626-8

### Time Use and Gender in Africa in Times of Structural Transformation

https://www.aeaweb.org/articles?id=10.1257/jep.36.1.57

* Modern US and African housewives work very similar home hours.
* Composition of home hours differs between modern US and African housewives.

## Epidemics and their Effects on the Economy

### COVID-Town: An Integrated Economic-Epidemiological Agent-Based Model

- Health care workers are exposed to a high risk of infection at work, since their work
  involves close contact with infected, especially if the hygienic measures at hospitals
  are not very strict.
- The COVID crisis has an impact both on supply and demand of the economy (see section
  7 for more details).
- Outbreaks in retirement homes are especially dangerous, since they involve the most
  vulnerable age groups.
- Early implementation of can reduce the number of infections and deaths (Huber and
  Langen 2020).
- Expansionary fiscal policy can constrain the economic losses (Deb et al. 2020).

### FOUR STYLIZED FACTS ABOUT COVID-19

- The growth rate of daily deaths from COVID-19 fell rapidly everywhere within the first
  30 days after each region reached 25 cumulative deaths.

- After this first period of rapid decline, the growth rate of daily deaths in all regions
  has hovered around zero or slightly below.

- The cross-regional standard deviation of growth rates of deaths fell rapidly in the
  first 10 days of the epidemic and has, subsequently, remained low relative to its
  initial level.

- When interpreted through a range of epidemiological models, Facts 1 - 3 imply that both
  the effective reproduction numbers and transmission rates of COVID- 19 fell rapidly from
  widely dispersed initial levels during the 30 days after cumulative deaths reached 25.
  After this initial period of rapid decline, the e↵ective reproduction number has hovered
  around one everywhere in the world.

### The complex interplay between COVID-19 and economic activity

- The evolution of the pandemics is waves-shaped;
- The evolution over time of new cases is a series of waves of easing amplitude;
- Peaks of new cases are almost always anticipated by peaks of GDP per capita.

### US Housing Market during COVID-19: Aggregate and Distributional Evidence

- After a temporary slow-down in March and April 2020, the median growth rate of the
  median housing price bounded back quickly and exceeded the growth rate before the
  COVID-19 crisis.

- The year-on-year growth rates of the median price per sqft in the recent four months (
  April-August 2020) have accelerated faster than any four-month period in the lead-up to
  the 2007-09 global financial crisis (GFC)

- The decreasing trend of the housing supply since mid-2019 is amplified by COVID-19.

- The hotness of housing demand, proxied by the online views per property, has been rising
  at an extraordinary rate since April 2020

- The response of housing demand to mortgage rate displays a structural break, which is
  robust to falsification tests

- The increase in the housing price is particularly strong at the lower-to-middle end of
  the zip code-level income distribution

- Price per Sqft Growth vs Median Family Income: Nonparametric Estimation

- The increase in the hotness of housing demand is particularly strong at the two ends of
  the zip code-level income distribution, displaying a U-shaped relationship

- The changes in housing price, demand, and supply since April 2020 are broad-based, with
  similar magnitudes of changes across metropolitan, micropolitan, small-town, and rural
  areas.

### Unmasking the Impact of COVID-19 on Businesses

- The COVID-19 shock has been severe and widespread across firms, with persistent negative
  impact on sales.
- The employment adjustment has operated mostly along the intensive margin (that is leave
  of absence and reduction in hours), with a small share of firms laying off workers.
- Smaller firms are disproportionately facing greater financial constraints.
- Firms are increasingly relying on digital solutions as a response to the shock.
- There is great uncertainty about the future, especially among firms that have
  experienced a larger drop in sales, which is associated with job losses.

## Classical facts from Economy

### Preston S., The changing relation between mortality and level of economic development, 1975 ,vol. 29  (pg. 231 -48) (Reprinted Int J Epidemiol.), as summarized by https://doi.org/10.1093/ije/dym079

- [...] is a strong, positive relationship between national income levels and life
  expectancy in poorer countries, though the relationship is non-linear as life expectancy
  levels in richer countries are less sensitive to variations in average income

- [...] the relationship is changing, with life expectancy increasing over time at all
  income levels.



## Best-cited papers from the first 10 result pages on Google Scholar using the search term '"stylized facts" "covid"'

Further criteria:

- disregard papers focused on the economy or finance
- only include papers where individual stylized facts are explicitly listed (not just extensive discussions of data or models)

### What does and does not correlate with COVID-19 death rates, CR Knittel, B Ozaltun

- First, the mode of commute correlated with the highest death rates is public transit.

- Second, depending on which model we focus on, there is evidence that all modes of commutes, other than biking, are associated with higher death rates relative to telecommuting.

### Four stylized facts about COVID-19 A Atkeson, K Kopecky, T Zha

- The growth rate of daily deaths from COVID-19 fell rapidly everywhere within the first
  30 days after each region reached 25 cumulative deaths.

- After this first period of rapid decline, the growth rate of daily deaths in all regions
  has hovered around zero or slightly below.

- The cross-regional standard deviation of growth rates of deaths fell rapidly in the
  first 10 days of the epidemic and has, subsequently, remained low relative to its
  initial level.

- When interpreted through a range of epidemiological models, Facts 1 - 3 imply that both
  the effective reproduction numbers and transmission rates of COVID- 19 fell rapidly from
  widely dispersed initial levels during the 30 days after cumulative deaths reached 25.
  After this initial period of rapid decline, the e↵ective reproduction number has hovered
  around one everywhere in the world.

### The Covid-19 epidemic in the UK, G Faggio, F Peracchi

- First, the number of deaths recorded each day (new deaths) are a small fraction of the number of cases confirmed each day under Pillar 1.

- Second, the profile of new deaths follows with some delay that of the confirmed new Pillar 1 cases and both are hump-shaped.

- Third, the confirmed new cases under Pillars 1 and 2 combined declines at a smaller rate relative to that of Pillar 1 only.


## Stylized facts demography

Search for "stylized facts demography" -> 5 most cited papers from the first 10 results pages

Other, higher cited publications excluded due to following reasons:
* No stylized facts explicitly mentioned
* Paper PDF not available
* The facts are not about the demographic but e.g. macroeconomic/financial facts (already in other category)
* Methodological papers
* firm or industry demographic

### Family structure and children's educational outcomes: Blended families, stylized facts, and descriptive regressions
516 Citations
* We found that children reared in traditional nuclear families also have substantially better educational outcomes than the joint children from stable blended families.
* Within stable blended families, the difference between he joint children and the stepchildren is neither substantial nor statistically significant.

### Life expectancy and economic growth: the role of the demographic transition
463 Citations
* after an initial increase, the natural rate of population growth eventually falls in association with reductions in mortality and fertility
*In these countries, which we call “post-transitional” in the remainder of this
paper, increases in life expectancy after 1940 went along with reductions in both fertility and population growth. In the other countries, which we call “pre-transitional”, improvements in life expectancy were associated with increases in population growth after 1940
* mortality has declined first and has been followed by declining fertility, and the result in each case has been a considerable acceleration in population growth”.
* The demography literature has proposed several criteria to identify whether a country has
reached the critical turning point of the demographic transition
	* Life expectancy at birth exceeds 50 years
	* Fertility or the crude birth rate has exhibited a sustained decline
	* The crude birth rate has fallen below the threshold of 30/1000

### Growth, distribution, and demography: Some lessons from history
270 Citations
* Mortality decline—especially infant and child mortality decline—marks the beginning of this stylized demographic transition, and fertility decline eventually follows with a lag, generating the rising rates of population growth
* it appears that changing demographic conditions might have ac-counted for even higher proportions of the trend acceleration from low growth rates prior to 1970 to the 6.1% per annum rate afterward
* resource-rich, labor-scarce countries under-went falling inequality

### Urbanization as a global historical process: Theory and evidence from sub‐Saharan Africa
242 Citations
* stylized facts of africa’s urban transition, namely the late onset of urbanization vis-à-vis other major less-developed world regions and the persistence of both urbanization and rapid urban  population  growth  in  the  late  twentieth  century  despite  economic stagnation
* mortality decline facilitates urban population growth directly by raising the rate of urban natural increase and indirectly by raising the rate of rural-to-urban migration.

### New evidence on the timing and spacing of births
175 Citations
* the younger the woman's age at marriage, the more rapid the pacing of subsequent fertility, and that those who begin childbearing early in their repro- ductive careers subsequently have children more rapidly.
* birth interval lengths depend little upon birth order, but far more upon the length of the previous interval