# Hard to quantify Stylized Facts

* Der Teil der SFs, der sich schwer in die Sprache integrieren lässt oder unklar ist, ist kursiv gedruckt.

## Cont2001    

https://doi.org/10.1088/1469-7688/1/2/304

Allgemein unklar

* *large drawdowns in stock prices and stock index values but not equally large
  upward movements*
* Coarse grained measures of volatility *predict* fine-scale volatility better
  than the other way around

Abhängigkeiten

Hier ist die frage, welche Variablenzuweisungen man zum Testen des SFs nehmen
würde. z.B. Welche time scales $\Delta t$ werden verwendet, um den 1. SF zu
testen.
 -> User gibt Range an, dann wird mit parameter scan darüber getestet.

* *With larger time scale $\Delta t$*, the asset distribution looks more and
  more like a normal distribution
* Linear autocorrelations of asset returns often insignificant except *for small
  time scales (~20 Min)*
* AC of absolute returns decays *as function of time lag* as powerlaw with
  $\beta \in [0.2,0]

## Fiorito1997

https://doi.org/10.5089/9781451927573.001

* Taxation being more uniform, national patterns emerge mostly from the
  spending side.
  * Ich verstehe die Aussage so: "Die 'spending side' unterscheidet sich in unterschiedlichen Ländern stark". Das ist schwer als SF zu quantifizieren (vorallem weil die Länder sich nicht in Zahlen ausdrücken lassen.)
  * -> Vmtl sind country-spendings einzelne Zeilen. Dann geht es nur noch um crosscorrelations
* The cyclical behavior of primary deficit does not differ from *overall
  balances*.
  * Was sind overall balances?
  * -> Sagt letztendlich primary deficit ist prozyklisch

## Challet2001 

https://doi.org/10.1016/S0378-4371(01)00103-0

* It is known that the volatility has *algebraically decaying auto-correlation*,
  and accordingly that the returns activity is clustered in time
  * Was bedeutet algebraically decaying auto correlation?
  * -> Algebraically kann ignoriert werdn
* Volatility auto-correlation is known to be related to *volume correlation*
  * Was ist volume correlation?
  * -> Crosscorrelation von zwei Autocorrelationen

## Knittel2020

* The mode of commute correlated with the highest death rates is public transit.
  * Sagt "Wenn variable commute=public transit, dann ist DR am höchsten.
  * Funktionalität fehlt noch in Sprache.
  * -> a) DR_public_transit, DR... sind Zeilen
  * -> oder b) brauchen filtering wie SELECT in sql
* Depending on which model we focus on, there is evidence that all modes of
  commutes, other than biking, are associated with higher death rates relative
  to telecommuting.
  * -> siehe SF davor

## Cervellati2011

https://doi.org/10.1007/s10887-011-9065-2

* after an initial increase, the natural rate of population growth eventually falls *in association with reductions in mortality and fertility*
  * Muss "association" getestet werden? Falls nicht, lässt sich dieser Fact beschreiben
* mortality has declined first and has been followed by declining fertility, and the result in each case has been a considerable acceleration in population growth”.
  * Könnte man in 2 SFs aufsplitten
* The demography literature has proposed several criteria to identify whether a country has reached the critical turning point of the *demographic transition*: a) Life expectancy at birth exceeds 50 years; b) Fertility or the crude birth rate has exhibited a sustained decline; c) The crude birth rate has fallen below the threshold of 30/1000
  * Kein wirklicher SF. Außer in den Daten oder dem Output steht, wann eine demographic transition passiert ist.

## Williamson1998

https://doi.org/10.1006/exeh.1998.0701

* it appears that changing demographic conditions might have ac-counted for even higher proportions of the trend acceleration from low growth rates prior to 1970 to the 6.1% per annum rate afterward

## Fox2012 

https://doi.org/10.1111/j.1728-4457.2012.00493.x

* stylized facts of africa’s urban transition, namely the late onset of urbanization vis-à-vis other major less-developed world regions and the persistence of both urbanization and rapid urban  population  growth  in  the  late  twentieth  century  despite  economic stagnation
  * inwiefern wird urbanization quantifiziert?
* mortality decline facilitates urban population growth directly by raising the rate of urban natural increase and indirectly by raising the rate of rural-to-urban migration.
  * Erster teil lässt sich vmtl Aussagen, aber die Erklärungen danach nicht mehr.