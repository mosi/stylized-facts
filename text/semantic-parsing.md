# Semantic Parsing

This document contains notes on semantic parsing and the corresponding challenges.

## We get

* Model output[s]
    * Timeseries of variables
    * Either 1 run or multiple runs
    * Need estimates if multiple runs given
* Stylized facts which argue about these timeseries'
    * Given in "Controlled Natural Language" CNL

## We need

1. Controlled Natural Language
2. Formal Specification FS as intermediate step that can be automatically induced from CNL
3. Test case generation/Statistical model checking procedures that can be automatically
   generated from FS

## Assumptions

### Do we allow non-parametric tests?

* Lorig et al. only focus on parametric tests on the following properties:
    * mean, proportion, variance
    * comparing means, proportions, variants
    * testing for differences
    * testing for correlation coefficients
* we need to allow for arbitrary distributions -> need non-parametric tests.
    * Allow for specifying assumptions such as normal distribution to allow for using
      faster tests?
        * probably just a nice-to-have feature

## Hypotheses specification

### Lorig et al

> **Hypothesis Specification:**
>
> * parameters -> hypotheses -> test specification
> * $(a=1 \land b=4 \land \#) -> \mu_1(c) \land h1(\mu_1 < 0) -> \delta=0.5$

1. Program gives input
2. Parameterizes model with first part
3. Simulation is run
4. User answers questions about assumption (normal distribution, variance known)
5. Program selects and executes significance test (e.g. t-test)
6. Used MASON (multi-agent simulation environment)

### Cam et al 2022

https://doi.org/10.2507/IJSIMM21-1-583

* extended SED-ML with hypothesis handling
* hypothesis consists of
    * listOfConditions
    * listOfExpressions
    * listOfOperators
    * listOfRelations
* expressions consist of a temporal operator and an expression string
* conditions consist of expressions, operators an a temporal operator
* use **ptSTL** to specify hypotheses

* [29] use STL for time series data analysis

### ptSTL (past time STL)

* var [<,>] constant
* logical expression
* **S** since, **P** previously, **A** always
* Chanlatte et al 2017 use STL to design features for time series data

## Related work in formal specification generation

* map controlled natural language CNL ($\subseteq$ English) converted to logic

## Related work in semantic parsing according to He et al. 2022

https://dl.acm.org/doi/pdf/10.1145/3510003.3510171
(paper does unconstrained -> STL)

* problem: context-sensitive language NL (e.g. english) -> context-free formal language TL
* many approaches [3–16]
* problem remains still open [9, (2019)]
* methods use:
    * predefined specification patterns [4, 8, 17]
    * restricted and more controlled natural language [6 , 12 , 18]
    * unconstrained:
        * NL expression -> intermediate representation -> result [3, 11, 13, 15]
        * Then apply predefined rules/macro mapping representation to TL expression
        * Mostly LTL [19]
* Available toolkits:
    * Wasp [22]
    * SEMPRE [23]
    * KRISP [24]
    * SippyCup [25]
    * Cornell Semantic Parsing [26]
* Also NL to
    * SQL queries [27-31]
    * Python code  [32]
    * bash commands [33]
    * other DSL [34]

## Related work papers

* based on abstract

| paper            | approach                                                    | usefulness |
|------------------|-------------------------------------------------------------|------------|
| Xiao2011         | use Syn. Tree Substitution Grammar, then learn on dataset   |            |
| Book (Ranta2011) | general, logic to NL translation and reduction              |            |
| Dwyer1999        | ReqAnalysis; patterns to describe finite-state verification |            |
| Yan2015          | ReqAnalysis; Use nlp parser and dictionary                  |            |
| Brunello2019     | Survey                                                      |            |
| Kress-Gazit2008  |                                                             |            |
| Autili2015       |                                                             |            |
| Lignos2015       |                                                             |            |
| Nikora2009       |                                                             |            |
| Ghosh2016        |                                                             |            |
| Fantechi1994     |                                                             |            |
| Dzifcak2009      |                                                             |            |
| Maler2012        |                                                             |            |
| Harris2015       |                                                             |            |

## Ranta2011

### GF Framework

Abstract syntax defines system of trees
Concrete syntax specifies how trees are generalized as strings
GF Grammars are reversible and possible ambiguous

```
abstract; constructs (EMUL x y) :
fun EMul : Exp -> Exp -> Exp
concrete: linearizes (EMul x y)
lin EMul x y = x ++ "*" ++ y
```

$x*y*z$ might lead to 2 diff. trees

### Basic translation of logic

Prop := proposition
Ind := Individual
Var := Variable

And, Or, If : Prop -> Prop -> Prop
Not : Prop -> Prop
Forall, Exist : Var -> Prop -> Prop
IVar : Var -> Ind
VStr : String -> Var

IInt : Int -> Ind
Add, Mul : Ind -> Ind -> Ind
Nat, Even, Odd : Ind -> Prop
Equal : Ind -> Ind -> Prop

then

for all x, if x is a natural number then x is even or x is odd

becomes

Forall (VStr "x") (If (Nat (IVar (VStr "x")))
(Or (Even (IVar (VStr "x"))) (Odd (IVar (VStr "x")))))

## Dwyer 1999

context

![](images/real-requirement.png)

Patterns:

* Occurence
    * **Absence**: state not in scope
    * **Existence**: state in scope
    * **Boundes Existence**: state at least [and at most] k times in scope
    * **Universality**: state always in scope
* Order
    * **Precedence**: Q precedes P in scope
    * **Response**: always $Q$ after P in scope
    * **Chain Precedence**: $P_1 P_2 .. P_m$ preceded by $Q_1 Q_2 .. Q_n$
    * **Chain Response**: $P_1 P_2 P_m$ followed by $Q_1 Q_2 .. Q_n$

Scopes:

```
- Global          |111111111111111111111
- Before Q        |11-------------------
- After Q         |--1111111111111111111
- Between Q and R |--11111--11111-------
- After Q until R |--11111--11111--11111
- Sequence        |  Q    R Q  Q R Q
```

("a given state/event occurs" := "a state in which the given state formula is
true, or an event from the given disjunction of events,
occurs.")

Did survey of 515 specifications and recorded

![](images/requirement.png)

## Yan2015

parse e.g.

* Req-08 If Air Ok signal remains low, auto-control mode is terminated
  in 3 seconds.
* Req-17 When auto-control mode is entered, eventually the cuff will
  be inflated.
* Req-28 If a valid pressure is unavailable in 180 seconds, manualmode
  should be triggered.
* Req-32 If pulse wave or arterial line is available, and cuff is
  selected, corroboration is triggered.
* Req-42 When auto-control mode is running, and the arterial line or
  pulse wave or cuff is lost, an alarm should sound in 60 seconds.
* Req-44 If pulse wave and arterial line are unavailable, and cuff is
  selected, and blood pressure is not valid, next manual mode is
  started.

Use LTL:

![](images/ltl.png)

Where

* X := next
* F := Eventually
* G := Globally (Always)
* U := Until

Structured English:

![](images/structured-english.png)

"where t is a time constant, * means the presence of
zero or more subcomponent, . means the composition of
different components, and [ ] means the optional components.
For noun, verb, participle, adjective and adverb, we do not
decompose them any more. The proposed grammar allows
extracting temporal operators according to the elements in
subordinator and modifier."

![](images/structured-english-tree.png)

## Brunello 2019

LTL from natural language survey

see <https://matthewbdwyer.github.io/psp/patterns/ltl.html>

##

"A Framework for the Assessment and Consolidation of Productivity Stylized Facts"

can't access

## Hypothesis Modelling

DOI: 10.1109/WSC.2017.8247880

### hypotheses

"According to Mill (1868), a hypothesis can be scientifically defined as "any
supposition which we make [...] in order to endeavour to deduce from it
conclusions in accordance with facts which are known to be real""

"For the description of the experiment itself, dedicated markup languages, e.g.,
SED-ML (Kohn and Le Novère 2008), and corresponding guidelines have been
proposed (Waltemath et al. 2011). Such approaches are complemented by ontologies
for describing scientific experiments (Soldatova and King 2006). Additionally,
DSL whose expressive power focuses on individual domains have been proposed for
describing experiments, e.g., SESSL (Ewald and Uhrmacher 2014) and the framework
proposed by Teran-Somohano et al. (2015)."

According to most simulation procedure models, the necessary first step of a
simulation study is the proper definition of the goal (Law and Kelton 1991,
Banks 1998). This is essential, as the study’s goal specifies the objective as
well as the purpose of the study.

"Yilmaz et al. (2016) distinguish between three types of experiment hypotheses
in simulation-based knowledge generation. In this work, only phenomenological
hypotheses are relevant as both mechanistic and control hypotheses make
statements about mechanisms of the model which are unavailable in the black box
approach pursued here."

To test phenomenological hypotheses, statistical hypothesis testing approaches
can be used.

## About target variables

most commonly additional quantitative criteria (performance measures) are
defined and used to compare the behavior of the model under different
parametrizations.

Performance indicators are goal-specific and often not part of the outputs of a
simulation model. Instead, output variables need to be mathematically combined
to new (target) variables which can then be used to assess the model’s
performance.

In the example used here, all output variables that represent non-value- added
times, i.a., wait time and queue time, need to be summed up to a new variable
manufacturing cycle time. Such (intermediate) variables are created artificially
with respect to assessing the performance of the model but are not directly used
as performance measures (  ̈Oren et al. 1984). As a next step, the output value
process time can be divided by the intermediate variable manufacturing cycle
time to receive the target variable manufacturing cycle efficiency.

* **parametrization of the model**:
    * specific values or ranges of values are assigned to the independent
      variables of the model, e.g. *#* Operator in FITS gives standard values to all
    * Parameter scanning may be better?

## FITS (previous paper by Lorig et al)

https://dl.acm.org/doi/abs/10.5555/3108905.3108923

## Chompsky hierarchy

* type 0 | Any
    * $\alpha$ -> $\beta$
* type 1 | Context-Sensitive
    * $\alpha A \beta$ -> $\alpha \gamma \beta (\alpha,\beta,\gamma := [bB]*)$
* type 2 | Context-Free
    * $A$ -> $[bB]$*
* type 3 | Regular
    * $A$ -> $b[B]$