# Papers

This document contains notes on different approaches on working with quantitative data

## Mitra2018 +

- describe qual data as $g_i(x) < 0$
- use Sum of Squares for quantitative $f_{quant}(x)$
- use static penalty $C_i * max(0,g_i(x))$ for qualitative $f_{qual}(x)$
- $f_{obj}(x) = f_{quant}(x) + f_{qual}(x)$
- use standard optimization algorithm for $f_{opt}$
- (used variant of scatter search in paper)

+: algorithm independant, allows soft/weighted constraints

-: rather simple, needs $g(x)<0$ constraints

## Mitra2020 +

- adapted objective function of previous approach to add possibility for *uncertainty
  quantification UQ*
- likelihood functions based on CDF of Gaussian Distribution (which describes $P(y<c)$ by
  definition)
- added model decrepancy to describe whether a sample is "outside scope of model" to
  diminish weight of anomalous observations
    - model decrepency implemented by giving random output independent of data sample some
      percent of the time

Code: [PyBioNetFit](https://github.com/lanl/PyBNF) (open source software)

## Piho2022 +

- added qualitative data to CTMCs
- qual. data specified as time logic formulas $\phi$
- quant. data transformed into logical specification $\phi_D$ as well
- then use rejection sampling to sample from posterior $p(\theta | \phi, \phi_D)$
- basically checks if both types of data are close enough to sample (?)
- qual. data in examples: $P_{t=[0, 10]}(A<B)$ and $P_{t=[0, 10]}(A<3)$

Code: none

+: high expressiveness (time logic), close to lecture ddmosi

-: needs language for CTMC and time logic, no code provided, stochastics heavy

## Schmiester 2020+21 +

- added qualitative data to ODEs
- typical meta modeling approach
- base model learns on quant. data
- then compute surrogate data which describes the best quant. representation of the qual.
  data
- choose n intervals $[x1, x1]$ by hand
- then use optimal scaling to learn corresponding interval boundaries [y1, y2]
  and clip data accordingly
- nests this surrogate data optimization into the model parameter optimization
    - hierarchical optimization problem
    - optimal scaling solves following formula

![](../images/optimal-scaling.png)

Code: available as tar.gz, use [pyPESTO](https://pypesto.readthedocs.io/en/latest/)
library

## Peters

### Stylized facts

modeling based on history might not be best; instead stylized facts:

- SF1: realGDP <> real investment. e.g. gdp rise follows investment spike
- SF2: recessions <--> density. (?)
- SF3: different cross correlations of macro Vars
- SF4: real GDP <> total investment. Similar to SF1
- SF5: different cross correlations of credit Vars
- SF6: Firm debt <> loan losses. cant identify clear correlation
- SF7: crisis duration right skewed. Hard to specify as rule
- SF8: Fiscal cost of banking crisis to GDP distribution is fat tailed. Hard to specify.
- SF9: Firm investment. (infrequent, large investments, periods with successive spikes and
  low or no investment activity))
- SF10: realGDP <-> Bankruptcy
- SF11: inflation <+> unemployment
- SF12: realGDP <-> unemployment
- SF13: Interbank market (interbank rate <-> excess reserves)

## Aral 2008 +

Improved Genetic Algorithm for typical Constrained optimization problem with linear
constraints and bounding limits for variables.

## Dentcheva 2006 +

- look at stochastic optimization problems and solutions.
- Mostly optimal solutions for convex and suboptimal for non-convex. Also some statistical
  approximation approaches
- very long (54 pages) and very theoretical -- in book "Probabilistic and Randomized
  Methods for Design under Uncertainty"

![](../images/stochastic-optimization1.png)
![](../images/stochastic-optimization2.png)

## Platt 1987 -

Add constraints to Neural Nets, very old

## Jannson2010 -

Estimation of parameters of typical COP. Economics background
Use Bayesian estimation approach

## Caunhye 2012 -

Review of COP optimization in emergency logistics

## Zhong 2014 -

Engineering example
fuzzy chance-constrained modelling approach
