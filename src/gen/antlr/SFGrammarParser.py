# Generated from antlr/SFGrammar.g4 by ANTLR 4.12.0
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO

def serializedATN():
    return [
        4,1,72,223,2,0,7,0,2,1,7,1,2,2,7,2,2,3,7,3,2,4,7,4,2,5,7,5,2,6,7,
        6,1,0,1,0,1,0,5,0,18,8,0,10,0,12,0,21,9,0,1,0,1,0,1,1,1,1,1,1,1,
        1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,5,1,41,8,1,10,
        1,12,1,44,9,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,5,1,55,8,1,10,
        1,12,1,58,9,1,1,1,1,1,1,1,3,1,63,8,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
        1,1,1,1,1,1,1,1,3,1,76,8,1,5,1,78,8,1,10,1,12,1,81,9,1,1,2,1,2,1,
        2,3,2,86,8,2,1,2,1,2,1,2,1,2,3,2,92,8,2,1,2,1,2,1,2,3,2,97,8,2,1,
        2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,
        2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,3,2,130,
        8,2,1,2,3,2,133,8,2,1,2,1,2,1,2,1,2,1,2,1,2,3,2,141,8,2,1,2,3,2,
        144,8,2,3,2,146,8,2,1,3,1,3,1,3,1,3,1,3,1,3,1,3,1,3,1,3,1,3,1,3,
        1,3,3,3,160,8,3,1,3,1,3,1,3,5,3,165,8,3,10,3,12,3,168,9,3,1,4,1,
        4,1,4,4,4,173,8,4,11,4,12,4,174,1,5,1,5,1,5,1,5,1,5,1,5,1,5,1,5,
        1,5,1,5,1,5,1,5,1,5,1,5,1,5,1,5,1,5,1,5,1,5,1,5,3,5,197,8,5,1,5,
        1,5,1,5,1,5,1,5,1,5,1,5,1,5,1,5,3,5,208,8,5,1,5,1,5,1,5,1,5,1,5,
        1,5,5,5,216,8,5,10,5,12,5,219,9,5,1,6,1,6,1,6,0,3,2,6,10,7,0,2,4,
        6,8,10,12,0,16,1,0,4,6,1,0,18,19,1,0,24,25,1,0,15,16,1,0,7,10,1,
        0,13,14,1,0,27,28,1,0,30,32,1,0,34,35,1,0,38,39,2,0,13,14,41,44,
        1,0,49,53,1,0,54,56,1,0,45,48,1,0,54,60,1,0,61,62,252,0,14,1,0,0,
        0,2,62,1,0,0,0,4,145,1,0,0,0,6,159,1,0,0,0,8,169,1,0,0,0,10,207,
        1,0,0,0,12,220,1,0,0,0,14,19,3,2,1,0,15,16,5,1,0,0,16,18,3,2,1,0,
        17,15,1,0,0,0,18,21,1,0,0,0,19,17,1,0,0,0,19,20,1,0,0,0,20,22,1,
        0,0,0,21,19,1,0,0,0,22,23,5,1,0,0,23,1,1,0,0,0,24,25,6,1,-1,0,25,
        63,3,4,2,0,26,27,5,2,0,0,27,28,3,2,1,0,28,29,5,3,0,0,29,63,1,0,0,
        0,30,31,7,0,0,0,31,63,3,2,1,6,32,33,5,17,0,0,33,63,3,2,1,3,34,35,
        7,1,0,0,35,36,5,67,0,0,36,37,5,20,0,0,37,42,5,67,0,0,38,39,5,21,
        0,0,39,41,5,67,0,0,40,38,1,0,0,0,41,44,1,0,0,0,42,40,1,0,0,0,42,
        43,1,0,0,0,43,45,1,0,0,0,44,42,1,0,0,0,45,46,5,22,0,0,46,63,3,2,
        1,2,47,48,5,23,0,0,48,49,7,2,0,0,49,50,5,67,0,0,50,51,5,20,0,0,51,
        56,5,67,0,0,52,53,5,21,0,0,53,55,5,67,0,0,54,52,1,0,0,0,55,58,1,
        0,0,0,56,54,1,0,0,0,56,57,1,0,0,0,57,59,1,0,0,0,58,56,1,0,0,0,59,
        60,5,26,0,0,60,61,7,2,0,0,61,63,3,10,5,0,62,24,1,0,0,0,62,26,1,0,
        0,0,62,30,1,0,0,0,62,32,1,0,0,0,62,34,1,0,0,0,62,47,1,0,0,0,63,79,
        1,0,0,0,64,65,10,4,0,0,65,66,7,3,0,0,66,78,3,2,1,5,67,68,10,5,0,
        0,68,69,7,4,0,0,69,75,3,2,1,0,70,71,5,11,0,0,71,72,5,67,0,0,72,73,
        5,12,0,0,73,74,5,67,0,0,74,76,7,5,0,0,75,70,1,0,0,0,75,76,1,0,0,
        0,76,78,1,0,0,0,77,64,1,0,0,0,77,67,1,0,0,0,78,81,1,0,0,0,79,77,
        1,0,0,0,79,80,1,0,0,0,80,3,1,0,0,0,81,79,1,0,0,0,82,83,3,6,3,0,83,
        85,7,6,0,0,84,86,5,71,0,0,85,84,1,0,0,0,85,86,1,0,0,0,86,87,1,0,
        0,0,87,91,5,69,0,0,88,89,5,29,0,0,89,90,5,67,0,0,90,92,7,5,0,0,91,
        88,1,0,0,0,91,92,1,0,0,0,92,146,1,0,0,0,93,94,3,10,5,0,94,96,5,27,
        0,0,95,97,5,71,0,0,96,95,1,0,0,0,96,97,1,0,0,0,97,98,1,0,0,0,98,
        99,5,70,0,0,99,146,1,0,0,0,100,101,3,6,3,0,101,102,7,7,0,0,102,103,
        3,6,3,0,103,146,1,0,0,0,104,105,3,6,3,0,105,106,7,7,0,0,106,107,
        5,67,0,0,107,146,1,0,0,0,108,109,3,10,5,0,109,110,7,7,0,0,110,111,
        5,67,0,0,111,146,1,0,0,0,112,113,3,10,5,0,113,114,7,7,0,0,114,115,
        3,10,5,0,115,146,1,0,0,0,116,117,3,10,5,0,117,118,5,33,0,0,118,119,
        5,67,0,0,119,120,5,16,0,0,120,121,5,67,0,0,121,146,1,0,0,0,122,123,
        3,6,3,0,123,124,7,6,0,0,124,125,7,8,0,0,125,129,5,36,0,0,126,127,
        5,37,0,0,127,130,7,9,0,0,128,130,5,12,0,0,129,126,1,0,0,0,129,128,
        1,0,0,0,129,130,1,0,0,0,130,132,1,0,0,0,131,133,3,6,3,0,132,131,
        1,0,0,0,132,133,1,0,0,0,133,140,1,0,0,0,134,135,5,40,0,0,135,141,
        5,67,0,0,136,137,5,11,0,0,137,138,5,67,0,0,138,139,5,12,0,0,139,
        141,5,67,0,0,140,134,1,0,0,0,140,136,1,0,0,0,140,141,1,0,0,0,141,
        143,1,0,0,0,142,144,7,10,0,0,143,142,1,0,0,0,143,144,1,0,0,0,144,
        146,1,0,0,0,145,82,1,0,0,0,145,93,1,0,0,0,145,100,1,0,0,0,145,104,
        1,0,0,0,145,108,1,0,0,0,145,112,1,0,0,0,145,116,1,0,0,0,145,122,
        1,0,0,0,146,5,1,0,0,0,147,148,6,3,-1,0,148,160,3,12,6,0,149,150,
        7,11,0,0,150,151,5,2,0,0,151,152,3,6,3,0,152,153,5,3,0,0,153,160,
        1,0,0,0,154,155,7,12,0,0,155,156,5,2,0,0,156,157,3,8,4,0,157,158,
        5,3,0,0,158,160,1,0,0,0,159,147,1,0,0,0,159,149,1,0,0,0,159,154,
        1,0,0,0,160,166,1,0,0,0,161,162,10,3,0,0,162,163,7,13,0,0,163,165,
        3,6,3,4,164,161,1,0,0,0,165,168,1,0,0,0,166,164,1,0,0,0,166,167,
        1,0,0,0,167,7,1,0,0,0,168,166,1,0,0,0,169,172,3,6,3,0,170,171,5,
        21,0,0,171,173,3,6,3,0,172,170,1,0,0,0,173,174,1,0,0,0,174,172,1,
        0,0,0,174,175,1,0,0,0,175,9,1,0,0,0,176,177,6,5,-1,0,177,178,7,14,
        0,0,178,179,5,2,0,0,179,180,3,6,3,0,180,181,5,3,0,0,181,208,1,0,
        0,0,182,183,7,15,0,0,183,184,5,2,0,0,184,185,3,10,5,0,185,186,5,
        21,0,0,186,187,3,10,5,0,187,188,5,3,0,0,188,208,1,0,0,0,189,190,
        5,64,0,0,190,191,3,6,3,0,191,192,5,21,0,0,192,196,3,6,3,0,193,194,
        5,21,0,0,194,195,5,65,0,0,195,197,5,67,0,0,196,193,1,0,0,0,196,197,
        1,0,0,0,197,198,1,0,0,0,198,199,5,3,0,0,199,208,1,0,0,0,200,201,
        5,66,0,0,201,202,3,6,3,0,202,203,5,21,0,0,203,204,5,65,0,0,204,205,
        5,67,0,0,205,206,5,3,0,0,206,208,1,0,0,0,207,176,1,0,0,0,207,182,
        1,0,0,0,207,189,1,0,0,0,207,200,1,0,0,0,208,217,1,0,0,0,209,210,
        10,4,0,0,210,211,7,13,0,0,211,216,3,10,5,5,212,213,10,3,0,0,213,
        214,5,63,0,0,214,216,3,10,5,4,215,209,1,0,0,0,215,212,1,0,0,0,216,
        219,1,0,0,0,217,215,1,0,0,0,217,218,1,0,0,0,218,11,1,0,0,0,219,217,
        1,0,0,0,220,221,5,68,0,0,221,13,1,0,0,0,22,19,42,56,62,75,77,79,
        85,91,96,129,132,140,143,145,159,166,174,196,207,215,217
    ]

class SFGrammarParser ( Parser ):

    grammarFileName = "SFGrammar.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "';'", "'('", "')'", "'Always'", "'Eventually'", 
                     "'Finally'", "'precedes'", "'follows'", "'until'", 
                     "'after'", "'within'", "'to'", "'days'", "'years'", 
                     "'or'", "'and'", "'not'", "'Forall'", "'Exists'", "' in {'", 
                     "','", "'}:'", "'The'", "'smaller'", "'larger'", "'} the'", 
                     "'is'", "'are'", "'for'", "'<'", "'>'", "'='", "'is between'", 
                     "'counter'", "'pro'", "'cyclical'", "'ly '", "'lagging'", 
                     "'leading'", "'by'", "'day'", "'year'", "'lag'", "'lags'", 
                     "'+'", "'-'", "'/'", "'*'", "'growthrate'", "'cumulated'", 
                     "'squared'", "'absolute'", "'volatility'", "'mean'", 
                     "'standard deviation'", "'variance'", "'first'", "'sum'", 
                     "'daily volatility'", "'kurtosis'", "'max'", "'min'", 
                     "'similar'", "'CC('", "'lag='", "'AC('" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "VALUE", "VAR", 
                      "COLUMN_DESCRIPTOR", "SCALAR_DESCRIPTOR", "STRENGTH", 
                      "WHITESPACE" ]

    RULE_expressions = 0
    RULE_expression = 1
    RULE_statement = 2
    RULE_column = 3
    RULE_columns = 4
    RULE_scalar = 5
    RULE_var = 6

    ruleNames =  [ "expressions", "expression", "statement", "column", "columns", 
                   "scalar", "var" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    T__6=7
    T__7=8
    T__8=9
    T__9=10
    T__10=11
    T__11=12
    T__12=13
    T__13=14
    T__14=15
    T__15=16
    T__16=17
    T__17=18
    T__18=19
    T__19=20
    T__20=21
    T__21=22
    T__22=23
    T__23=24
    T__24=25
    T__25=26
    T__26=27
    T__27=28
    T__28=29
    T__29=30
    T__30=31
    T__31=32
    T__32=33
    T__33=34
    T__34=35
    T__35=36
    T__36=37
    T__37=38
    T__38=39
    T__39=40
    T__40=41
    T__41=42
    T__42=43
    T__43=44
    T__44=45
    T__45=46
    T__46=47
    T__47=48
    T__48=49
    T__49=50
    T__50=51
    T__51=52
    T__52=53
    T__53=54
    T__54=55
    T__55=56
    T__56=57
    T__57=58
    T__58=59
    T__59=60
    T__60=61
    T__61=62
    T__62=63
    T__63=64
    T__64=65
    T__65=66
    VALUE=67
    VAR=68
    COLUMN_DESCRIPTOR=69
    SCALAR_DESCRIPTOR=70
    STRENGTH=71
    WHITESPACE=72

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.12.0")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class ExpressionsContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SFGrammarParser.ExpressionContext)
            else:
                return self.getTypedRuleContext(SFGrammarParser.ExpressionContext,i)


        def getRuleIndex(self):
            return SFGrammarParser.RULE_expressions

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpressions" ):
                listener.enterExpressions(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpressions" ):
                listener.exitExpressions(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpressions" ):
                return visitor.visitExpressions(self)
            else:
                return visitor.visitChildren(self)




    def expressions(self):

        localctx = SFGrammarParser.ExpressionsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_expressions)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 14
            self.expression(0)
            self.state = 19
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,0,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 15
                    self.match(SFGrammarParser.T__0)
                    self.state = 16
                    self.expression(0) 
                self.state = 21
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,0,self._ctx)

            self.state = 22
            self.match(SFGrammarParser.T__0)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExpressionContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.interval = -1


        def getRuleIndex(self):
            return SFGrammarParser.RULE_expression

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)
            self.interval = ctx.interval


    class BracketedExpContext(ExpressionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SFGrammarParser.ExpressionContext
            super().__init__(parser)
            self.x = None # ExpressionContext
            self.copyFrom(ctx)

        def expression(self):
            return self.getTypedRuleContext(SFGrammarParser.ExpressionContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBracketedExp" ):
                listener.enterBracketedExp(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBracketedExp" ):
                listener.exitBracketedExp(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBracketedExp" ):
                return visitor.visitBracketedExp(self)
            else:
                return visitor.visitChildren(self)


    class QuantifierExpContext(ExpressionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SFGrammarParser.ExpressionContext
            super().__init__(parser)
            self.quantifier = None # Token
            self.name = None # Token
            self.x = None # ExpressionContext
            self.copyFrom(ctx)

        def VALUE(self, i:int=None):
            if i is None:
                return self.getTokens(SFGrammarParser.VALUE)
            else:
                return self.getToken(SFGrammarParser.VALUE, i)
        def expression(self):
            return self.getTypedRuleContext(SFGrammarParser.ExpressionContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterQuantifierExp" ):
                listener.enterQuantifierExp(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitQuantifierExp" ):
                listener.exitQuantifierExp(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitQuantifierExp" ):
                return visitor.visitQuantifierExp(self)
            else:
                return visitor.visitChildren(self)


    class StatementExpContext(ExpressionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SFGrammarParser.ExpressionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def statement(self):
            return self.getTypedRuleContext(SFGrammarParser.StatementContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStatementExp" ):
                listener.enterStatementExp(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStatementExp" ):
                listener.exitStatementExp(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStatementExp" ):
                return visitor.visitStatementExp(self)
            else:
                return visitor.visitChildren(self)


    class BinaryLogicExpContext(ExpressionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SFGrammarParser.ExpressionContext
            super().__init__(parser)
            self.x = None # ExpressionContext
            self.op = None # Token
            self.y = None # ExpressionContext
            self.copyFrom(ctx)

        def expression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SFGrammarParser.ExpressionContext)
            else:
                return self.getTypedRuleContext(SFGrammarParser.ExpressionContext,i)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBinaryLogicExp" ):
                listener.enterBinaryLogicExp(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBinaryLogicExp" ):
                listener.exitBinaryLogicExp(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBinaryLogicExp" ):
                return visitor.visitBinaryLogicExp(self)
            else:
                return visitor.visitChildren(self)


    class UnaryLTLExpContext(ExpressionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SFGrammarParser.ExpressionContext
            super().__init__(parser)
            self.op = None # Token
            self.x = None # ExpressionContext
            self.copyFrom(ctx)

        def expression(self):
            return self.getTypedRuleContext(SFGrammarParser.ExpressionContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterUnaryLTLExp" ):
                listener.enterUnaryLTLExp(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitUnaryLTLExp" ):
                listener.exitUnaryLTLExp(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitUnaryLTLExp" ):
                return visitor.visitUnaryLTLExp(self)
            else:
                return visitor.visitChildren(self)


    class RankedQuantifierExpContext(ExpressionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SFGrammarParser.ExpressionContext
            super().__init__(parser)
            self.comp1 = None # Token
            self.name = None # Token
            self.comp2 = None # Token
            self.x = None # ScalarContext
            self.copyFrom(ctx)

        def VALUE(self, i:int=None):
            if i is None:
                return self.getTokens(SFGrammarParser.VALUE)
            else:
                return self.getToken(SFGrammarParser.VALUE, i)
        def scalar(self):
            return self.getTypedRuleContext(SFGrammarParser.ScalarContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRankedQuantifierExp" ):
                listener.enterRankedQuantifierExp(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRankedQuantifierExp" ):
                listener.exitRankedQuantifierExp(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRankedQuantifierExp" ):
                return visitor.visitRankedQuantifierExp(self)
            else:
                return visitor.visitChildren(self)


    class BinaryLTLExpContext(ExpressionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SFGrammarParser.ExpressionContext
            super().__init__(parser)
            self.x = None # ExpressionContext
            self.op = None # Token
            self.y = None # ExpressionContext
            self.lowerbound = None # Token
            self.upperbound = None # Token
            self.copyFrom(ctx)

        def expression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SFGrammarParser.ExpressionContext)
            else:
                return self.getTypedRuleContext(SFGrammarParser.ExpressionContext,i)

        def VALUE(self, i:int=None):
            if i is None:
                return self.getTokens(SFGrammarParser.VALUE)
            else:
                return self.getToken(SFGrammarParser.VALUE, i)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBinaryLTLExp" ):
                listener.enterBinaryLTLExp(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBinaryLTLExp" ):
                listener.exitBinaryLTLExp(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBinaryLTLExp" ):
                return visitor.visitBinaryLTLExp(self)
            else:
                return visitor.visitChildren(self)


    class UnaryLogicExpContext(ExpressionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SFGrammarParser.ExpressionContext
            super().__init__(parser)
            self.op = None # Token
            self.x = None # ExpressionContext
            self.copyFrom(ctx)

        def expression(self):
            return self.getTypedRuleContext(SFGrammarParser.ExpressionContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterUnaryLogicExp" ):
                listener.enterUnaryLogicExp(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitUnaryLogicExp" ):
                listener.exitUnaryLogicExp(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitUnaryLogicExp" ):
                return visitor.visitUnaryLogicExp(self)
            else:
                return visitor.visitChildren(self)



    def expression(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = SFGrammarParser.ExpressionContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 2
        self.enterRecursionRule(localctx, 2, self.RULE_expression, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 62
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 64, 66, 68]:
                localctx = SFGrammarParser.StatementExpContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx

                self.state = 25
                self.statement()
                pass
            elif token in [2]:
                localctx = SFGrammarParser.BracketedExpContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 26
                self.match(SFGrammarParser.T__1)
                self.state = 27
                localctx.x = self.expression(0)
                self.state = 28
                self.match(SFGrammarParser.T__2)
                pass
            elif token in [4, 5, 6]:
                localctx = SFGrammarParser.UnaryLTLExpContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 30
                localctx.op = self._input.LT(1)
                _la = self._input.LA(1)
                if not((((_la) & ~0x3f) == 0 and ((1 << _la) & 112) != 0)):
                    localctx.op = self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 31
                localctx.x = self.expression(6)
                pass
            elif token in [17]:
                localctx = SFGrammarParser.UnaryLogicExpContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 32
                localctx.op = self.match(SFGrammarParser.T__16)
                self.state = 33
                localctx.x = self.expression(3)
                pass
            elif token in [18, 19]:
                localctx = SFGrammarParser.QuantifierExpContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 34
                localctx.quantifier = self._input.LT(1)
                _la = self._input.LA(1)
                if not(_la==18 or _la==19):
                    localctx.quantifier = self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 35
                localctx.name = self.match(SFGrammarParser.VALUE)
                self.state = 36
                self.match(SFGrammarParser.T__19)
                self.state = 37
                self.match(SFGrammarParser.VALUE)
                self.state = 42
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==21:
                    self.state = 38
                    self.match(SFGrammarParser.T__20)
                    self.state = 39
                    self.match(SFGrammarParser.VALUE)
                    self.state = 44
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 45
                self.match(SFGrammarParser.T__21)
                self.state = 46
                localctx.x = self.expression(2)
                pass
            elif token in [23]:
                localctx = SFGrammarParser.RankedQuantifierExpContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 47
                self.match(SFGrammarParser.T__22)
                self.state = 48
                localctx.comp1 = self._input.LT(1)
                _la = self._input.LA(1)
                if not(_la==24 or _la==25):
                    localctx.comp1 = self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 49
                localctx.name = self.match(SFGrammarParser.VALUE)
                self.state = 50
                self.match(SFGrammarParser.T__19)
                self.state = 51
                self.match(SFGrammarParser.VALUE)
                self.state = 56
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==21:
                    self.state = 52
                    self.match(SFGrammarParser.T__20)
                    self.state = 53
                    self.match(SFGrammarParser.VALUE)
                    self.state = 58
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 59
                self.match(SFGrammarParser.T__25)
                self.state = 60
                localctx.comp2 = self._input.LT(1)
                _la = self._input.LA(1)
                if not(_la==24 or _la==25):
                    localctx.comp2 = self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 61
                localctx.x = self.scalar(0)
                pass
            else:
                raise NoViableAltException(self)

            self._ctx.stop = self._input.LT(-1)
            self.state = 79
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,6,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 77
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,5,self._ctx)
                    if la_ == 1:
                        localctx = SFGrammarParser.BinaryLogicExpContext(self, SFGrammarParser.ExpressionContext(self, _parentctx, _parentState))
                        localctx.x = _prevctx
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expression)
                        self.state = 64
                        if not self.precpred(self._ctx, 4):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 4)")
                        self.state = 65
                        localctx.op = self._input.LT(1)
                        _la = self._input.LA(1)
                        if not(_la==15 or _la==16):
                            localctx.op = self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 66
                        localctx.y = self.expression(5)
                        pass

                    elif la_ == 2:
                        localctx = SFGrammarParser.BinaryLTLExpContext(self, SFGrammarParser.ExpressionContext(self, _parentctx, _parentState))
                        localctx.x = _prevctx
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expression)
                        self.state = 67
                        if not self.precpred(self._ctx, 5):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 5)")
                        self.state = 68
                        localctx.op = self._input.LT(1)
                        _la = self._input.LA(1)
                        if not((((_la) & ~0x3f) == 0 and ((1 << _la) & 1920) != 0)):
                            localctx.op = self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 69
                        localctx.y = self.expression(0)
                        self.state = 75
                        self._errHandler.sync(self)
                        la_ = self._interp.adaptivePredict(self._input,4,self._ctx)
                        if la_ == 1:
                            self.state = 70
                            self.match(SFGrammarParser.T__10)
                            self.state = 71
                            localctx.lowerbound = self.match(SFGrammarParser.VALUE)
                            self.state = 72
                            self.match(SFGrammarParser.T__11)
                            self.state = 73
                            localctx.upperbound = self.match(SFGrammarParser.VALUE)
                            self.state = 74
                            _la = self._input.LA(1)
                            if not(_la==13 or _la==14):
                                self._errHandler.recoverInline(self)
                            else:
                                self._errHandler.reportMatch(self)
                                self.consume()


                        pass

             
                self.state = 81
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,6,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class StatementContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return SFGrammarParser.RULE_statement

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class ScalarComparisonContext(StatementContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SFGrammarParser.StatementContext
            super().__init__(parser)
            self.x = None # ScalarContext
            self.op = None # Token
            self.val = None # Token
            self.copyFrom(ctx)

        def scalar(self):
            return self.getTypedRuleContext(SFGrammarParser.ScalarContext,0)

        def VALUE(self):
            return self.getToken(SFGrammarParser.VALUE, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterScalarComparison" ):
                listener.enterScalarComparison(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitScalarComparison" ):
                listener.exitScalarComparison(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitScalarComparison" ):
                return visitor.visitScalarComparison(self)
            else:
                return visitor.visitChildren(self)


    class ScalarToScalarComparisonContext(StatementContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SFGrammarParser.StatementContext
            super().__init__(parser)
            self.x = None # ScalarContext
            self.op = None # Token
            self.y = None # ScalarContext
            self.copyFrom(ctx)

        def scalar(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SFGrammarParser.ScalarContext)
            else:
                return self.getTypedRuleContext(SFGrammarParser.ScalarContext,i)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterScalarToScalarComparison" ):
                listener.enterScalarToScalarComparison(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitScalarToScalarComparison" ):
                listener.exitScalarToScalarComparison(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitScalarToScalarComparison" ):
                return visitor.visitScalarToScalarComparison(self)
            else:
                return visitor.visitChildren(self)


    class ColumnDescriptionContext(StatementContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SFGrammarParser.StatementContext
            super().__init__(parser)
            self.x = None # ColumnContext
            self.str_ = None # Token
            self.desc = None # Token
            self.length = None # Token
            self.copyFrom(ctx)

        def column(self):
            return self.getTypedRuleContext(SFGrammarParser.ColumnContext,0)

        def COLUMN_DESCRIPTOR(self):
            return self.getToken(SFGrammarParser.COLUMN_DESCRIPTOR, 0)
        def STRENGTH(self):
            return self.getToken(SFGrammarParser.STRENGTH, 0)
        def VALUE(self):
            return self.getToken(SFGrammarParser.VALUE, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterColumnDescription" ):
                listener.enterColumnDescription(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitColumnDescription" ):
                listener.exitColumnDescription(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitColumnDescription" ):
                return visitor.visitColumnDescription(self)
            else:
                return visitor.visitChildren(self)


    class ColumnToColumnComparisonContext(StatementContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SFGrammarParser.StatementContext
            super().__init__(parser)
            self.x = None # ColumnContext
            self.op = None # Token
            self.y = None # ColumnContext
            self.copyFrom(ctx)

        def column(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SFGrammarParser.ColumnContext)
            else:
                return self.getTypedRuleContext(SFGrammarParser.ColumnContext,i)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterColumnToColumnComparison" ):
                listener.enterColumnToColumnComparison(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitColumnToColumnComparison" ):
                listener.exitColumnToColumnComparison(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitColumnToColumnComparison" ):
                return visitor.visitColumnToColumnComparison(self)
            else:
                return visitor.visitChildren(self)


    class CyclicalityStatementContext(StatementContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SFGrammarParser.StatementContext
            super().__init__(parser)
            self.x = None # ColumnContext
            self.y = None # ColumnContext
            self.lag = None # Token
            self.lowerbound = None # Token
            self.upperbound = None # Token
            self.copyFrom(ctx)

        def column(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SFGrammarParser.ColumnContext)
            else:
                return self.getTypedRuleContext(SFGrammarParser.ColumnContext,i)

        def VALUE(self, i:int=None):
            if i is None:
                return self.getTokens(SFGrammarParser.VALUE)
            else:
                return self.getToken(SFGrammarParser.VALUE, i)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCyclicalityStatement" ):
                listener.enterCyclicalityStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCyclicalityStatement" ):
                listener.exitCyclicalityStatement(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCyclicalityStatement" ):
                return visitor.visitCyclicalityStatement(self)
            else:
                return visitor.visitChildren(self)


    class ScalarDescriptionContext(StatementContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SFGrammarParser.StatementContext
            super().__init__(parser)
            self.x = None # ScalarContext
            self.str_ = None # Token
            self.desc = None # Token
            self.copyFrom(ctx)

        def scalar(self):
            return self.getTypedRuleContext(SFGrammarParser.ScalarContext,0)

        def SCALAR_DESCRIPTOR(self):
            return self.getToken(SFGrammarParser.SCALAR_DESCRIPTOR, 0)
        def STRENGTH(self):
            return self.getToken(SFGrammarParser.STRENGTH, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterScalarDescription" ):
                listener.enterScalarDescription(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitScalarDescription" ):
                listener.exitScalarDescription(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitScalarDescription" ):
                return visitor.visitScalarDescription(self)
            else:
                return visitor.visitChildren(self)


    class ColumnToScalarComparisonContext(StatementContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SFGrammarParser.StatementContext
            super().__init__(parser)
            self.x = None # ColumnContext
            self.op = None # Token
            self.val = None # Token
            self.copyFrom(ctx)

        def column(self):
            return self.getTypedRuleContext(SFGrammarParser.ColumnContext,0)

        def VALUE(self):
            return self.getToken(SFGrammarParser.VALUE, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterColumnToScalarComparison" ):
                listener.enterColumnToScalarComparison(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitColumnToScalarComparison" ):
                listener.exitColumnToScalarComparison(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitColumnToScalarComparison" ):
                return visitor.visitColumnToScalarComparison(self)
            else:
                return visitor.visitChildren(self)


    class ScalarRangeComparisonContext(StatementContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SFGrammarParser.StatementContext
            super().__init__(parser)
            self.x = None # ScalarContext
            self.lower = None # Token
            self.upper = None # Token
            self.copyFrom(ctx)

        def scalar(self):
            return self.getTypedRuleContext(SFGrammarParser.ScalarContext,0)

        def VALUE(self, i:int=None):
            if i is None:
                return self.getTokens(SFGrammarParser.VALUE)
            else:
                return self.getToken(SFGrammarParser.VALUE, i)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterScalarRangeComparison" ):
                listener.enterScalarRangeComparison(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitScalarRangeComparison" ):
                listener.exitScalarRangeComparison(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitScalarRangeComparison" ):
                return visitor.visitScalarRangeComparison(self)
            else:
                return visitor.visitChildren(self)



    def statement(self):

        localctx = SFGrammarParser.StatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_statement)
        self._la = 0 # Token type
        try:
            self.state = 145
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,14,self._ctx)
            if la_ == 1:
                localctx = SFGrammarParser.ColumnDescriptionContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 82
                localctx.x = self.column(0)
                self.state = 83
                _la = self._input.LA(1)
                if not(_la==27 or _la==28):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 85
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==71:
                    self.state = 84
                    localctx.str_ = self.match(SFGrammarParser.STRENGTH)


                self.state = 87
                localctx.desc = self.match(SFGrammarParser.COLUMN_DESCRIPTOR)
                self.state = 91
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,8,self._ctx)
                if la_ == 1:
                    self.state = 88
                    self.match(SFGrammarParser.T__28)
                    self.state = 89
                    localctx.length = self.match(SFGrammarParser.VALUE)
                    self.state = 90
                    _la = self._input.LA(1)
                    if not(_la==13 or _la==14):
                        self._errHandler.recoverInline(self)
                    else:
                        self._errHandler.reportMatch(self)
                        self.consume()


                pass

            elif la_ == 2:
                localctx = SFGrammarParser.ScalarDescriptionContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 93
                localctx.x = self.scalar(0)
                self.state = 94
                self.match(SFGrammarParser.T__26)
                self.state = 96
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==71:
                    self.state = 95
                    localctx.str_ = self.match(SFGrammarParser.STRENGTH)


                self.state = 98
                localctx.desc = self.match(SFGrammarParser.SCALAR_DESCRIPTOR)
                pass

            elif la_ == 3:
                localctx = SFGrammarParser.ColumnToColumnComparisonContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 100
                localctx.x = self.column(0)
                self.state = 101
                localctx.op = self._input.LT(1)
                _la = self._input.LA(1)
                if not((((_la) & ~0x3f) == 0 and ((1 << _la) & 7516192768) != 0)):
                    localctx.op = self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 102
                localctx.y = self.column(0)
                pass

            elif la_ == 4:
                localctx = SFGrammarParser.ColumnToScalarComparisonContext(self, localctx)
                self.enterOuterAlt(localctx, 4)
                self.state = 104
                localctx.x = self.column(0)
                self.state = 105
                localctx.op = self._input.LT(1)
                _la = self._input.LA(1)
                if not((((_la) & ~0x3f) == 0 and ((1 << _la) & 7516192768) != 0)):
                    localctx.op = self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 106
                localctx.val = self.match(SFGrammarParser.VALUE)
                pass

            elif la_ == 5:
                localctx = SFGrammarParser.ScalarComparisonContext(self, localctx)
                self.enterOuterAlt(localctx, 5)
                self.state = 108
                localctx.x = self.scalar(0)
                self.state = 109
                localctx.op = self._input.LT(1)
                _la = self._input.LA(1)
                if not((((_la) & ~0x3f) == 0 and ((1 << _la) & 7516192768) != 0)):
                    localctx.op = self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 110
                localctx.val = self.match(SFGrammarParser.VALUE)
                pass

            elif la_ == 6:
                localctx = SFGrammarParser.ScalarToScalarComparisonContext(self, localctx)
                self.enterOuterAlt(localctx, 6)
                self.state = 112
                localctx.x = self.scalar(0)
                self.state = 113
                localctx.op = self._input.LT(1)
                _la = self._input.LA(1)
                if not((((_la) & ~0x3f) == 0 and ((1 << _la) & 7516192768) != 0)):
                    localctx.op = self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 114
                localctx.y = self.scalar(0)
                pass

            elif la_ == 7:
                localctx = SFGrammarParser.ScalarRangeComparisonContext(self, localctx)
                self.enterOuterAlt(localctx, 7)
                self.state = 116
                localctx.x = self.scalar(0)
                self.state = 117
                self.match(SFGrammarParser.T__32)
                self.state = 118
                localctx.lower = self.match(SFGrammarParser.VALUE)
                self.state = 119
                self.match(SFGrammarParser.T__15)
                self.state = 120
                localctx.upper = self.match(SFGrammarParser.VALUE)
                pass

            elif la_ == 8:
                localctx = SFGrammarParser.CyclicalityStatementContext(self, localctx)
                self.enterOuterAlt(localctx, 8)
                self.state = 122
                localctx.x = self.column(0)
                self.state = 123
                _la = self._input.LA(1)
                if not(_la==27 or _la==28):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 124
                _la = self._input.LA(1)
                if not(_la==34 or _la==35):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 125
                self.match(SFGrammarParser.T__35)
                self.state = 129
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,10,self._ctx)
                if la_ == 1:
                    self.state = 126
                    self.match(SFGrammarParser.T__36)
                    self.state = 127
                    _la = self._input.LA(1)
                    if not(_la==38 or _la==39):
                        self._errHandler.recoverInline(self)
                    else:
                        self._errHandler.reportMatch(self)
                        self.consume()

                elif la_ == 2:
                    self.state = 128
                    self.match(SFGrammarParser.T__11)


                self.state = 132
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,11,self._ctx)
                if la_ == 1:
                    self.state = 131
                    localctx.y = self.column(0)


                self.state = 140
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,12,self._ctx)
                if la_ == 1:
                    self.state = 134
                    self.match(SFGrammarParser.T__39)
                    self.state = 135
                    localctx.lag = self.match(SFGrammarParser.VALUE)

                elif la_ == 2:
                    self.state = 136
                    self.match(SFGrammarParser.T__10)
                    self.state = 137
                    localctx.lowerbound = self.match(SFGrammarParser.VALUE)
                    self.state = 138
                    self.match(SFGrammarParser.T__11)
                    self.state = 139
                    localctx.upperbound = self.match(SFGrammarParser.VALUE)


                self.state = 143
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,13,self._ctx)
                if la_ == 1:
                    self.state = 142
                    _la = self._input.LA(1)
                    if not((((_la) & ~0x3f) == 0 and ((1 << _la) & 32985348857856) != 0)):
                        self._errHandler.recoverInline(self)
                    else:
                        self._errHandler.reportMatch(self)
                        self.consume()


                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ColumnContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return SFGrammarParser.RULE_column

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)


    class RowwiseColumnContext(ColumnContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SFGrammarParser.ColumnContext
            super().__init__(parser)
            self.op = None # Token
            self.xs = None # ColumnsContext
            self.copyFrom(ctx)

        def columns(self):
            return self.getTypedRuleContext(SFGrammarParser.ColumnsContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRowwiseColumn" ):
                listener.enterRowwiseColumn(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRowwiseColumn" ):
                listener.exitRowwiseColumn(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRowwiseColumn" ):
                return visitor.visitRowwiseColumn(self)
            else:
                return visitor.visitChildren(self)


    class UnivariateColumnContext(ColumnContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SFGrammarParser.ColumnContext
            super().__init__(parser)
            self.op = None # Token
            self.x = None # ColumnContext
            self.copyFrom(ctx)

        def column(self):
            return self.getTypedRuleContext(SFGrammarParser.ColumnContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterUnivariateColumn" ):
                listener.enterUnivariateColumn(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitUnivariateColumn" ):
                listener.exitUnivariateColumn(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitUnivariateColumn" ):
                return visitor.visitUnivariateColumn(self)
            else:
                return visitor.visitChildren(self)


    class AtomicColumnContext(ColumnContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SFGrammarParser.ColumnContext
            super().__init__(parser)
            self.atom = None # VarContext
            self.copyFrom(ctx)

        def var(self):
            return self.getTypedRuleContext(SFGrammarParser.VarContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAtomicColumn" ):
                listener.enterAtomicColumn(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAtomicColumn" ):
                listener.exitAtomicColumn(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAtomicColumn" ):
                return visitor.visitAtomicColumn(self)
            else:
                return visitor.visitChildren(self)


    class BivariateColumnContext(ColumnContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SFGrammarParser.ColumnContext
            super().__init__(parser)
            self.x = None # ColumnContext
            self.op = None # Token
            self.y = None # ColumnContext
            self.copyFrom(ctx)

        def column(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SFGrammarParser.ColumnContext)
            else:
                return self.getTypedRuleContext(SFGrammarParser.ColumnContext,i)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBivariateColumn" ):
                listener.enterBivariateColumn(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBivariateColumn" ):
                listener.exitBivariateColumn(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBivariateColumn" ):
                return visitor.visitBivariateColumn(self)
            else:
                return visitor.visitChildren(self)



    def column(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = SFGrammarParser.ColumnContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 6
        self.enterRecursionRule(localctx, 6, self.RULE_column, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 159
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [68]:
                localctx = SFGrammarParser.AtomicColumnContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx

                self.state = 148
                localctx.atom = self.var()
                pass
            elif token in [49, 50, 51, 52, 53]:
                localctx = SFGrammarParser.UnivariateColumnContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 149
                localctx.op = self._input.LT(1)
                _la = self._input.LA(1)
                if not((((_la) & ~0x3f) == 0 and ((1 << _la) & 17451448556060672) != 0)):
                    localctx.op = self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 150
                self.match(SFGrammarParser.T__1)
                self.state = 151
                localctx.x = self.column(0)
                self.state = 152
                self.match(SFGrammarParser.T__2)
                pass
            elif token in [54, 55, 56]:
                localctx = SFGrammarParser.RowwiseColumnContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 154
                localctx.op = self._input.LT(1)
                _la = self._input.LA(1)
                if not((((_la) & ~0x3f) == 0 and ((1 << _la) & 126100789566373888) != 0)):
                    localctx.op = self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 155
                self.match(SFGrammarParser.T__1)
                self.state = 156
                localctx.xs = self.columns()
                self.state = 157
                self.match(SFGrammarParser.T__2)
                pass
            else:
                raise NoViableAltException(self)

            self._ctx.stop = self._input.LT(-1)
            self.state = 166
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,16,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = SFGrammarParser.BivariateColumnContext(self, SFGrammarParser.ColumnContext(self, _parentctx, _parentState))
                    localctx.x = _prevctx
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_column)
                    self.state = 161
                    if not self.precpred(self._ctx, 3):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 3)")
                    self.state = 162
                    localctx.op = self._input.LT(1)
                    _la = self._input.LA(1)
                    if not((((_la) & ~0x3f) == 0 and ((1 << _la) & 527765581332480) != 0)):
                        localctx.op = self._errHandler.recoverInline(self)
                    else:
                        self._errHandler.reportMatch(self)
                        self.consume()
                    self.state = 163
                    localctx.y = self.column(4) 
                self.state = 168
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,16,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class ColumnsContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def column(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SFGrammarParser.ColumnContext)
            else:
                return self.getTypedRuleContext(SFGrammarParser.ColumnContext,i)


        def getRuleIndex(self):
            return SFGrammarParser.RULE_columns

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterColumns" ):
                listener.enterColumns(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitColumns" ):
                listener.exitColumns(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitColumns" ):
                return visitor.visitColumns(self)
            else:
                return visitor.visitChildren(self)




    def columns(self):

        localctx = SFGrammarParser.ColumnsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_columns)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 169
            self.column(0)
            self.state = 172 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 170
                self.match(SFGrammarParser.T__20)
                self.state = 171
                self.column(0)
                self.state = 174 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==21):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ScalarContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return SFGrammarParser.RULE_scalar

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)


    class CrosscorrScalarContext(ScalarContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SFGrammarParser.ScalarContext
            super().__init__(parser)
            self.x = None # ColumnContext
            self.y = None # ColumnContext
            self.lag = None # Token
            self.copyFrom(ctx)

        def column(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SFGrammarParser.ColumnContext)
            else:
                return self.getTypedRuleContext(SFGrammarParser.ColumnContext,i)

        def VALUE(self):
            return self.getToken(SFGrammarParser.VALUE, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCrosscorrScalar" ):
                listener.enterCrosscorrScalar(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCrosscorrScalar" ):
                listener.exitCrosscorrScalar(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCrosscorrScalar" ):
                return visitor.visitCrosscorrScalar(self)
            else:
                return visitor.visitChildren(self)


    class AutocorrScalarContext(ScalarContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SFGrammarParser.ScalarContext
            super().__init__(parser)
            self.x = None # ColumnContext
            self.lag = None # Token
            self.copyFrom(ctx)

        def column(self):
            return self.getTypedRuleContext(SFGrammarParser.ColumnContext,0)

        def VALUE(self):
            return self.getToken(SFGrammarParser.VALUE, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAutocorrScalar" ):
                listener.enterAutocorrScalar(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAutocorrScalar" ):
                listener.exitAutocorrScalar(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAutocorrScalar" ):
                return visitor.visitAutocorrScalar(self)
            else:
                return visitor.visitChildren(self)


    class ClosenessScalarContext(ScalarContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SFGrammarParser.ScalarContext
            super().__init__(parser)
            self.x = None # ScalarContext
            self.y = None # ScalarContext
            self.copyFrom(ctx)

        def scalar(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SFGrammarParser.ScalarContext)
            else:
                return self.getTypedRuleContext(SFGrammarParser.ScalarContext,i)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterClosenessScalar" ):
                listener.enterClosenessScalar(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitClosenessScalar" ):
                listener.exitClosenessScalar(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitClosenessScalar" ):
                return visitor.visitClosenessScalar(self)
            else:
                return visitor.visitChildren(self)


    class BivariateScalarContext(ScalarContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SFGrammarParser.ScalarContext
            super().__init__(parser)
            self.op = None # Token
            self.x = None # ScalarContext
            self.y = None # ScalarContext
            self.copyFrom(ctx)

        def scalar(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SFGrammarParser.ScalarContext)
            else:
                return self.getTypedRuleContext(SFGrammarParser.ScalarContext,i)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBivariateScalar" ):
                listener.enterBivariateScalar(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBivariateScalar" ):
                listener.exitBivariateScalar(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBivariateScalar" ):
                return visitor.visitBivariateScalar(self)
            else:
                return visitor.visitChildren(self)


    class UnivariateScalarContext(ScalarContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SFGrammarParser.ScalarContext
            super().__init__(parser)
            self.op = None # Token
            self.x = None # ColumnContext
            self.copyFrom(ctx)

        def column(self):
            return self.getTypedRuleContext(SFGrammarParser.ColumnContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterUnivariateScalar" ):
                listener.enterUnivariateScalar(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitUnivariateScalar" ):
                listener.exitUnivariateScalar(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitUnivariateScalar" ):
                return visitor.visitUnivariateScalar(self)
            else:
                return visitor.visitChildren(self)


    class ArithmeticScalarContext(ScalarContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SFGrammarParser.ScalarContext
            super().__init__(parser)
            self.x = None # ScalarContext
            self.op = None # Token
            self.y = None # ScalarContext
            self.copyFrom(ctx)

        def scalar(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SFGrammarParser.ScalarContext)
            else:
                return self.getTypedRuleContext(SFGrammarParser.ScalarContext,i)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterArithmeticScalar" ):
                listener.enterArithmeticScalar(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitArithmeticScalar" ):
                listener.exitArithmeticScalar(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitArithmeticScalar" ):
                return visitor.visitArithmeticScalar(self)
            else:
                return visitor.visitChildren(self)



    def scalar(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = SFGrammarParser.ScalarContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 10
        self.enterRecursionRule(localctx, 10, self.RULE_scalar, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 207
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [54, 55, 56, 57, 58, 59, 60]:
                localctx = SFGrammarParser.UnivariateScalarContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx

                self.state = 177
                localctx.op = self._input.LT(1)
                _la = self._input.LA(1)
                if not((((_la) & ~0x3f) == 0 and ((1 << _la) & 2287828610704211968) != 0)):
                    localctx.op = self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 178
                self.match(SFGrammarParser.T__1)
                self.state = 179
                localctx.x = self.column(0)
                self.state = 180
                self.match(SFGrammarParser.T__2)
                pass
            elif token in [61, 62]:
                localctx = SFGrammarParser.BivariateScalarContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 182
                localctx.op = self._input.LT(1)
                _la = self._input.LA(1)
                if not(_la==61 or _la==62):
                    localctx.op = self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 183
                self.match(SFGrammarParser.T__1)
                self.state = 184
                localctx.x = self.scalar(0)
                self.state = 185
                self.match(SFGrammarParser.T__20)
                self.state = 186
                localctx.y = self.scalar(0)
                self.state = 187
                self.match(SFGrammarParser.T__2)
                pass
            elif token in [64]:
                localctx = SFGrammarParser.CrosscorrScalarContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 189
                self.match(SFGrammarParser.T__63)
                self.state = 190
                localctx.x = self.column(0)
                self.state = 191
                self.match(SFGrammarParser.T__20)
                self.state = 192
                localctx.y = self.column(0)
                self.state = 196
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==21:
                    self.state = 193
                    self.match(SFGrammarParser.T__20)
                    self.state = 194
                    self.match(SFGrammarParser.T__64)
                    self.state = 195
                    localctx.lag = self.match(SFGrammarParser.VALUE)


                self.state = 198
                self.match(SFGrammarParser.T__2)
                pass
            elif token in [66]:
                localctx = SFGrammarParser.AutocorrScalarContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 200
                self.match(SFGrammarParser.T__65)
                self.state = 201
                localctx.x = self.column(0)
                self.state = 202
                self.match(SFGrammarParser.T__20)
                self.state = 203
                self.match(SFGrammarParser.T__64)
                self.state = 204
                localctx.lag = self.match(SFGrammarParser.VALUE)
                self.state = 205
                self.match(SFGrammarParser.T__2)
                pass
            else:
                raise NoViableAltException(self)

            self._ctx.stop = self._input.LT(-1)
            self.state = 217
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,21,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 215
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,20,self._ctx)
                    if la_ == 1:
                        localctx = SFGrammarParser.ArithmeticScalarContext(self, SFGrammarParser.ScalarContext(self, _parentctx, _parentState))
                        localctx.x = _prevctx
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_scalar)
                        self.state = 209
                        if not self.precpred(self._ctx, 4):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 4)")
                        self.state = 210
                        localctx.op = self._input.LT(1)
                        _la = self._input.LA(1)
                        if not((((_la) & ~0x3f) == 0 and ((1 << _la) & 527765581332480) != 0)):
                            localctx.op = self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 211
                        localctx.y = self.scalar(5)
                        pass

                    elif la_ == 2:
                        localctx = SFGrammarParser.ClosenessScalarContext(self, SFGrammarParser.ScalarContext(self, _parentctx, _parentState))
                        localctx.x = _prevctx
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_scalar)
                        self.state = 212
                        if not self.precpred(self._ctx, 3):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 3)")
                        self.state = 213
                        self.match(SFGrammarParser.T__62)
                        self.state = 214
                        localctx.y = self.scalar(4)
                        pass

             
                self.state = 219
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,21,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class VarContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.interval = -1
            self.col = None # Token

        def VAR(self):
            return self.getToken(SFGrammarParser.VAR, 0)

        def getRuleIndex(self):
            return SFGrammarParser.RULE_var

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterVar" ):
                listener.enterVar(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitVar" ):
                listener.exitVar(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVar" ):
                return visitor.visitVar(self)
            else:
                return visitor.visitChildren(self)




    def var(self):

        localctx = SFGrammarParser.VarContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_var)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 220
            localctx.col = self.match(SFGrammarParser.VAR)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[1] = self.expression_sempred
        self._predicates[3] = self.column_sempred
        self._predicates[5] = self.scalar_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def expression_sempred(self, localctx:ExpressionContext, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 4)
         

            if predIndex == 1:
                return self.precpred(self._ctx, 5)
         

    def column_sempred(self, localctx:ColumnContext, predIndex:int):
            if predIndex == 2:
                return self.precpred(self._ctx, 3)
         

    def scalar_sempred(self, localctx:ScalarContext, predIndex:int):
            if predIndex == 3:
                return self.precpred(self._ctx, 4)
         

            if predIndex == 4:
                return self.precpred(self._ctx, 3)
         




