# Generated from antlr/SFGrammar.g4 by ANTLR 4.12.0
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .SFGrammarParser import SFGrammarParser
else:
    from SFGrammarParser import SFGrammarParser

# This class defines a complete listener for a parse tree produced by SFGrammarParser.
class SFGrammarListener(ParseTreeListener):

    # Enter a parse tree produced by SFGrammarParser#expressions.
    def enterExpressions(self, ctx:SFGrammarParser.ExpressionsContext):
        pass

    # Exit a parse tree produced by SFGrammarParser#expressions.
    def exitExpressions(self, ctx:SFGrammarParser.ExpressionsContext):
        pass


    # Enter a parse tree produced by SFGrammarParser#bracketedExp.
    def enterBracketedExp(self, ctx:SFGrammarParser.BracketedExpContext):
        pass

    # Exit a parse tree produced by SFGrammarParser#bracketedExp.
    def exitBracketedExp(self, ctx:SFGrammarParser.BracketedExpContext):
        pass


    # Enter a parse tree produced by SFGrammarParser#quantifierExp.
    def enterQuantifierExp(self, ctx:SFGrammarParser.QuantifierExpContext):
        pass

    # Exit a parse tree produced by SFGrammarParser#quantifierExp.
    def exitQuantifierExp(self, ctx:SFGrammarParser.QuantifierExpContext):
        pass


    # Enter a parse tree produced by SFGrammarParser#statementExp.
    def enterStatementExp(self, ctx:SFGrammarParser.StatementExpContext):
        pass

    # Exit a parse tree produced by SFGrammarParser#statementExp.
    def exitStatementExp(self, ctx:SFGrammarParser.StatementExpContext):
        pass


    # Enter a parse tree produced by SFGrammarParser#binaryLogicExp.
    def enterBinaryLogicExp(self, ctx:SFGrammarParser.BinaryLogicExpContext):
        pass

    # Exit a parse tree produced by SFGrammarParser#binaryLogicExp.
    def exitBinaryLogicExp(self, ctx:SFGrammarParser.BinaryLogicExpContext):
        pass


    # Enter a parse tree produced by SFGrammarParser#unaryLTLExp.
    def enterUnaryLTLExp(self, ctx:SFGrammarParser.UnaryLTLExpContext):
        pass

    # Exit a parse tree produced by SFGrammarParser#unaryLTLExp.
    def exitUnaryLTLExp(self, ctx:SFGrammarParser.UnaryLTLExpContext):
        pass


    # Enter a parse tree produced by SFGrammarParser#rankedQuantifierExp.
    def enterRankedQuantifierExp(self, ctx:SFGrammarParser.RankedQuantifierExpContext):
        pass

    # Exit a parse tree produced by SFGrammarParser#rankedQuantifierExp.
    def exitRankedQuantifierExp(self, ctx:SFGrammarParser.RankedQuantifierExpContext):
        pass


    # Enter a parse tree produced by SFGrammarParser#binaryLTLExp.
    def enterBinaryLTLExp(self, ctx:SFGrammarParser.BinaryLTLExpContext):
        pass

    # Exit a parse tree produced by SFGrammarParser#binaryLTLExp.
    def exitBinaryLTLExp(self, ctx:SFGrammarParser.BinaryLTLExpContext):
        pass


    # Enter a parse tree produced by SFGrammarParser#unaryLogicExp.
    def enterUnaryLogicExp(self, ctx:SFGrammarParser.UnaryLogicExpContext):
        pass

    # Exit a parse tree produced by SFGrammarParser#unaryLogicExp.
    def exitUnaryLogicExp(self, ctx:SFGrammarParser.UnaryLogicExpContext):
        pass


    # Enter a parse tree produced by SFGrammarParser#columnDescription.
    def enterColumnDescription(self, ctx:SFGrammarParser.ColumnDescriptionContext):
        pass

    # Exit a parse tree produced by SFGrammarParser#columnDescription.
    def exitColumnDescription(self, ctx:SFGrammarParser.ColumnDescriptionContext):
        pass


    # Enter a parse tree produced by SFGrammarParser#scalarDescription.
    def enterScalarDescription(self, ctx:SFGrammarParser.ScalarDescriptionContext):
        pass

    # Exit a parse tree produced by SFGrammarParser#scalarDescription.
    def exitScalarDescription(self, ctx:SFGrammarParser.ScalarDescriptionContext):
        pass


    # Enter a parse tree produced by SFGrammarParser#columnToColumnComparison.
    def enterColumnToColumnComparison(self, ctx:SFGrammarParser.ColumnToColumnComparisonContext):
        pass

    # Exit a parse tree produced by SFGrammarParser#columnToColumnComparison.
    def exitColumnToColumnComparison(self, ctx:SFGrammarParser.ColumnToColumnComparisonContext):
        pass


    # Enter a parse tree produced by SFGrammarParser#columnToScalarComparison.
    def enterColumnToScalarComparison(self, ctx:SFGrammarParser.ColumnToScalarComparisonContext):
        pass

    # Exit a parse tree produced by SFGrammarParser#columnToScalarComparison.
    def exitColumnToScalarComparison(self, ctx:SFGrammarParser.ColumnToScalarComparisonContext):
        pass


    # Enter a parse tree produced by SFGrammarParser#scalarComparison.
    def enterScalarComparison(self, ctx:SFGrammarParser.ScalarComparisonContext):
        pass

    # Exit a parse tree produced by SFGrammarParser#scalarComparison.
    def exitScalarComparison(self, ctx:SFGrammarParser.ScalarComparisonContext):
        pass


    # Enter a parse tree produced by SFGrammarParser#scalarToScalarComparison.
    def enterScalarToScalarComparison(self, ctx:SFGrammarParser.ScalarToScalarComparisonContext):
        pass

    # Exit a parse tree produced by SFGrammarParser#scalarToScalarComparison.
    def exitScalarToScalarComparison(self, ctx:SFGrammarParser.ScalarToScalarComparisonContext):
        pass


    # Enter a parse tree produced by SFGrammarParser#scalarRangeComparison.
    def enterScalarRangeComparison(self, ctx:SFGrammarParser.ScalarRangeComparisonContext):
        pass

    # Exit a parse tree produced by SFGrammarParser#scalarRangeComparison.
    def exitScalarRangeComparison(self, ctx:SFGrammarParser.ScalarRangeComparisonContext):
        pass


    # Enter a parse tree produced by SFGrammarParser#cyclicalityStatement.
    def enterCyclicalityStatement(self, ctx:SFGrammarParser.CyclicalityStatementContext):
        pass

    # Exit a parse tree produced by SFGrammarParser#cyclicalityStatement.
    def exitCyclicalityStatement(self, ctx:SFGrammarParser.CyclicalityStatementContext):
        pass


    # Enter a parse tree produced by SFGrammarParser#rowwiseColumn.
    def enterRowwiseColumn(self, ctx:SFGrammarParser.RowwiseColumnContext):
        pass

    # Exit a parse tree produced by SFGrammarParser#rowwiseColumn.
    def exitRowwiseColumn(self, ctx:SFGrammarParser.RowwiseColumnContext):
        pass


    # Enter a parse tree produced by SFGrammarParser#univariateColumn.
    def enterUnivariateColumn(self, ctx:SFGrammarParser.UnivariateColumnContext):
        pass

    # Exit a parse tree produced by SFGrammarParser#univariateColumn.
    def exitUnivariateColumn(self, ctx:SFGrammarParser.UnivariateColumnContext):
        pass


    # Enter a parse tree produced by SFGrammarParser#atomicColumn.
    def enterAtomicColumn(self, ctx:SFGrammarParser.AtomicColumnContext):
        pass

    # Exit a parse tree produced by SFGrammarParser#atomicColumn.
    def exitAtomicColumn(self, ctx:SFGrammarParser.AtomicColumnContext):
        pass


    # Enter a parse tree produced by SFGrammarParser#bivariateColumn.
    def enterBivariateColumn(self, ctx:SFGrammarParser.BivariateColumnContext):
        pass

    # Exit a parse tree produced by SFGrammarParser#bivariateColumn.
    def exitBivariateColumn(self, ctx:SFGrammarParser.BivariateColumnContext):
        pass


    # Enter a parse tree produced by SFGrammarParser#columns.
    def enterColumns(self, ctx:SFGrammarParser.ColumnsContext):
        pass

    # Exit a parse tree produced by SFGrammarParser#columns.
    def exitColumns(self, ctx:SFGrammarParser.ColumnsContext):
        pass


    # Enter a parse tree produced by SFGrammarParser#crosscorrScalar.
    def enterCrosscorrScalar(self, ctx:SFGrammarParser.CrosscorrScalarContext):
        pass

    # Exit a parse tree produced by SFGrammarParser#crosscorrScalar.
    def exitCrosscorrScalar(self, ctx:SFGrammarParser.CrosscorrScalarContext):
        pass


    # Enter a parse tree produced by SFGrammarParser#autocorrScalar.
    def enterAutocorrScalar(self, ctx:SFGrammarParser.AutocorrScalarContext):
        pass

    # Exit a parse tree produced by SFGrammarParser#autocorrScalar.
    def exitAutocorrScalar(self, ctx:SFGrammarParser.AutocorrScalarContext):
        pass


    # Enter a parse tree produced by SFGrammarParser#closenessScalar.
    def enterClosenessScalar(self, ctx:SFGrammarParser.ClosenessScalarContext):
        pass

    # Exit a parse tree produced by SFGrammarParser#closenessScalar.
    def exitClosenessScalar(self, ctx:SFGrammarParser.ClosenessScalarContext):
        pass


    # Enter a parse tree produced by SFGrammarParser#bivariateScalar.
    def enterBivariateScalar(self, ctx:SFGrammarParser.BivariateScalarContext):
        pass

    # Exit a parse tree produced by SFGrammarParser#bivariateScalar.
    def exitBivariateScalar(self, ctx:SFGrammarParser.BivariateScalarContext):
        pass


    # Enter a parse tree produced by SFGrammarParser#univariateScalar.
    def enterUnivariateScalar(self, ctx:SFGrammarParser.UnivariateScalarContext):
        pass

    # Exit a parse tree produced by SFGrammarParser#univariateScalar.
    def exitUnivariateScalar(self, ctx:SFGrammarParser.UnivariateScalarContext):
        pass


    # Enter a parse tree produced by SFGrammarParser#arithmeticScalar.
    def enterArithmeticScalar(self, ctx:SFGrammarParser.ArithmeticScalarContext):
        pass

    # Exit a parse tree produced by SFGrammarParser#arithmeticScalar.
    def exitArithmeticScalar(self, ctx:SFGrammarParser.ArithmeticScalarContext):
        pass


    # Enter a parse tree produced by SFGrammarParser#var.
    def enterVar(self, ctx:SFGrammarParser.VarContext):
        pass

    # Exit a parse tree produced by SFGrammarParser#var.
    def exitVar(self, ctx:SFGrammarParser.VarContext):
        pass



del SFGrammarParser