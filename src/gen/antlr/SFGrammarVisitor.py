# Generated from antlr/SFGrammar.g4 by ANTLR 4.12.0
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .SFGrammarParser import SFGrammarParser
else:
    from SFGrammarParser import SFGrammarParser

# This class defines a complete generic visitor for a parse tree produced by SFGrammarParser.

class SFGrammarVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by SFGrammarParser#expressions.
    def visitExpressions(self, ctx:SFGrammarParser.ExpressionsContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SFGrammarParser#bracketedExp.
    def visitBracketedExp(self, ctx:SFGrammarParser.BracketedExpContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SFGrammarParser#quantifierExp.
    def visitQuantifierExp(self, ctx:SFGrammarParser.QuantifierExpContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SFGrammarParser#statementExp.
    def visitStatementExp(self, ctx:SFGrammarParser.StatementExpContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SFGrammarParser#binaryLogicExp.
    def visitBinaryLogicExp(self, ctx:SFGrammarParser.BinaryLogicExpContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SFGrammarParser#unaryLTLExp.
    def visitUnaryLTLExp(self, ctx:SFGrammarParser.UnaryLTLExpContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SFGrammarParser#rankedQuantifierExp.
    def visitRankedQuantifierExp(self, ctx:SFGrammarParser.RankedQuantifierExpContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SFGrammarParser#binaryLTLExp.
    def visitBinaryLTLExp(self, ctx:SFGrammarParser.BinaryLTLExpContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SFGrammarParser#unaryLogicExp.
    def visitUnaryLogicExp(self, ctx:SFGrammarParser.UnaryLogicExpContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SFGrammarParser#columnDescription.
    def visitColumnDescription(self, ctx:SFGrammarParser.ColumnDescriptionContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SFGrammarParser#scalarDescription.
    def visitScalarDescription(self, ctx:SFGrammarParser.ScalarDescriptionContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SFGrammarParser#columnToColumnComparison.
    def visitColumnToColumnComparison(self, ctx:SFGrammarParser.ColumnToColumnComparisonContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SFGrammarParser#columnToScalarComparison.
    def visitColumnToScalarComparison(self, ctx:SFGrammarParser.ColumnToScalarComparisonContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SFGrammarParser#scalarComparison.
    def visitScalarComparison(self, ctx:SFGrammarParser.ScalarComparisonContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SFGrammarParser#scalarToScalarComparison.
    def visitScalarToScalarComparison(self, ctx:SFGrammarParser.ScalarToScalarComparisonContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SFGrammarParser#scalarRangeComparison.
    def visitScalarRangeComparison(self, ctx:SFGrammarParser.ScalarRangeComparisonContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SFGrammarParser#cyclicalityStatement.
    def visitCyclicalityStatement(self, ctx:SFGrammarParser.CyclicalityStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SFGrammarParser#rowwiseColumn.
    def visitRowwiseColumn(self, ctx:SFGrammarParser.RowwiseColumnContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SFGrammarParser#univariateColumn.
    def visitUnivariateColumn(self, ctx:SFGrammarParser.UnivariateColumnContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SFGrammarParser#atomicColumn.
    def visitAtomicColumn(self, ctx:SFGrammarParser.AtomicColumnContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SFGrammarParser#bivariateColumn.
    def visitBivariateColumn(self, ctx:SFGrammarParser.BivariateColumnContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SFGrammarParser#columns.
    def visitColumns(self, ctx:SFGrammarParser.ColumnsContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SFGrammarParser#crosscorrScalar.
    def visitCrosscorrScalar(self, ctx:SFGrammarParser.CrosscorrScalarContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SFGrammarParser#autocorrScalar.
    def visitAutocorrScalar(self, ctx:SFGrammarParser.AutocorrScalarContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SFGrammarParser#closenessScalar.
    def visitClosenessScalar(self, ctx:SFGrammarParser.ClosenessScalarContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SFGrammarParser#bivariateScalar.
    def visitBivariateScalar(self, ctx:SFGrammarParser.BivariateScalarContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SFGrammarParser#univariateScalar.
    def visitUnivariateScalar(self, ctx:SFGrammarParser.UnivariateScalarContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SFGrammarParser#arithmeticScalar.
    def visitArithmeticScalar(self, ctx:SFGrammarParser.ArithmeticScalarContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SFGrammarParser#var.
    def visitVar(self, ctx:SFGrammarParser.VarContext):
        return self.visitChildren(ctx)



del SFGrammarParser