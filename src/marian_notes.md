# Notes

## Diskussionspunkte

* A is True after B is falling for 5 years
  * Soll B hier vollständig gegolten haben und danach erst A
  * Bisher: A is True after B is falling for 5 years within 5 to 5 years
* A after (Eventually B)
  * Soll B hier vor A gelten?

* (Eventually B) after A

## Mögliche nächste Schritte

* Volatility (Cont Paper)
* Parameter rausschreiben und Konfiguration erlauben
* Smoothing implementieren
* Versuchen, die übersetzten SFs irgendwie zu testen
* SFs schreiben von anderen
* SFs an Florian Modell testen

## Todo

* A after B soll gelten, wenn B irgendwann mal zuvor galt und nicht A gilt direkt dann, wenn B aufhört zu gelten.
* paper lesen <https://www.econstor.eu/bitstream/10419/4303/1/KWP_1426_Stochastic_Behavioral_Asset_pricing_Models.pdf>
* Smoothing vor LTL Tests notwendig?
* Schwierigkeiten notieren
* Parameter notieren

## Notizen/Schwierigkeiten

* Viele Annahmen bei Implementation & viele kleine Parameter, die SF Validierung beeinflussen.
  * Z.B. ab welcher Steigung gilt "x is growing/falling"
* Bei einigen SFs unklar, ob Übersetzung == Aussage
* Frage des Smoothings/Correcting for Noise
* Todo: Nested LTL? (Not supported yet)
* 2D LTLs: Welche sind für uns interessant?
* Release, Strong Release, Weak Release, Until
* Sollen wir auch den Lag bestimmen bei "procyclical" -> range angeben möglich
* x is growing after x is falling
* Vorschlag: Nur SFs über Time series betrachten. Also e.g. nicht "Return distribution is like normal distribution
* x after y within 30 days

## Demographic Transition SF

In these countries, which we call "post-transitional" in the remainder of this paper, increases in life expectancy after 1940 went along with reductions in both fertility and population growth. In the other countries, which we call "pre-transitional", improvements in life expectancy were associated with increases in population growth after 1940 

The demography literature has proposed several criteria to identify whether a country has reached the critical turning point of the demographic transition: a) Life expectancy at birth exceeds 50 years; b) Fertility or the crude birth rate has exhibited a sustained decline; c) The crude birth rate has fallen below the threshold of 30/1000        |

### Einzelne Schritte

* POST = _expectancy is countercyclically lagging _fertility and _expectancy is countercyclically lagging _population_growth
* PRE = _expectancy is procyclically lagging _population_growth
* A = life_expectancy > 50
* B = Eventually _fertility is declining for 5 years or _CBR is declining for 5 years
* C = _CBR < 0.3

### Wenn sich transition nicht ändert

* ((_has_transitioned = 1 and POST) or (_has_transitioned = 0 and PRE)) after _year=1940

### Daraus ergibt sich

* (((POST after (A and C)) after (B and PRE)) after _year = 1940

### Probleme

* Geht davon aus, dass B nur in der Zeit vor POST getestet wird.
* Noch nicht ganz richtig: POST muss nun gar nicht gelten, weil A und C immer gilt
* Alternative: (((POST and (A and C)) after (B and PRE)) after _year = 1940
* Auch nicht ganz richtig, weil jetzt A und C direkt nach B und PRE gelten muss

### Notizen

(_life_expectancy > 50 and _CBR < 0.03) after (_fertility is declining for 5 years or _CBR is declining for 5 years) within 5 to 20 years    

(((_expectancy is countercyclically lagging _fertility and _expectancy is countercyclically lagging _population_growth and (life_expectancy > 50 and _CBR < 0.3)) after (Eventually _fertility is declining for 5 years or _CBR is declining for 5 years and _expectancy is procyclically lagging _population_growth))
