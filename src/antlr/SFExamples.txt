_linear_no_noise is strict monotonically increasing;
_linear is strict monotonically increasing;
_linear_no_noise is monotonically increasing;
_linear_no_noise is monotonically decreasing;

Eventually Always _life_expectancy > 50;
Eventually _CBR < 0.3;
Eventually _fertility is falling for 5 years or _CBR is falling for 5 years;
Eventually _life_expectancy is procyclically lagging _population_growth;
Eventually (_life_expectancy is countercyclically lagging _fertility and _life_expectancy is countercyclically lagging _population_growth);
(_life_expectancy is countercyclically lagging _fertility and _life_expectancy is countercyclically lagging _population_growth and _life_expectancy > 50 and _CBR < 0.3) after (Eventually (_fertility is falling for 5 years or _CBR is falling for 5 years) and _life_expectancy is procyclically lagging _population_growth);

(_life_expectancy > 50 and _CBR < 0.3) after (_fertility is falling for 5 years or _CBR is falling for 5 years) within 5 to 20 years;

(_linear_no_noise > 20 after _linear_no_noise = 20);
(_linear_no_noise > 20 after _linear_no_noise = 20) after _linear_no_noise < 20;
(Eventually _linear > 20) after _linear > 10;

_powerlaw is heavy-tailed;
not _normal is heavy-tailed;
not _normal is light-tailed;
not _exponential is heavy-tailed;

kurtosis(_normal) is between -1 and 1;
kurtosis(_normal) is between 1 and 2;
kurtosis(_normal) is between -0.1 and 0.1;
kurtosis(_exponential) is between -2.6 and 3.4;

mean(_scaled, _sin) is growing;
variance(_scaled, _linear) is growing;
variance(_sin, _negative_sin) is flat;

_sin is procyclically lagging;
_sin is procyclically lagging _negative_sin;
_sin is countercyclical;
_sin is countercyclically lagging _sin within 6 to 8 days;
_sin is procyclically lagging within 15 to 15 days;
_sin is procyclically lagging within 22 to 22 days;
_sin is procyclically lagging _sin within 28 to 32 days;
_sin is procyclically lagging _sin within -15 to -15 days;

Eventually _u is growing;
Always _u is growing;
_u is growing after _u is falling;
_u is growing after _u is falling within 20 to 40 days;
(_u is growing or _u is flat) after (_u is falling or _u is flat);

(_u is growing) after (_u is falling) within 20 to 30 days;

CC(_passengers, _passengers, lag=12) is strong positive;
CC(_passengers, _passengers, lag=3) is strong positive;

Forall <lag> in {-1, 12, 24, 35}: AC(_passengers,lag=<lag>) is strong positive;
Forall <lag> in {3, -4, 24, 35}: AC(_passengers,lag=<lag>) is strong positive;
Exists <lag> in {3, -4, 24, 35}: AC(_passengers,lag=<lag>) is strong positive;

The larger <lag> in {1,2,3,4} the smaller AC(_passengers,lag=<lag>);
The larger <lag> in {1,2,3,4} the larger AC(_passengers,lag=<lag>);

AC(_passengers,lag=3) is positive;
AC(_passengers,lag=3) is strong positive;
CC(_sin,_negative_sin) is negative;
CC(_linear,_u) is close to zero;
AC(_passengers,lag=12) is close to zero;
_passengers is non-stationary;
_sin is stationary;
growthrate(_u) is flat for 50 days after _linear < 40;
_u is growing for 50 days after _linear < 40;
