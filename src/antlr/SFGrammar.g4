grammar SFGrammar;

expressions: expression (';' expression)* ';';

expression locals [int interval=-1]
    : statement # statementExp
    | '(' x=expression ')' # bracketedExp
    | op=('Always'|'Eventually'|'Finally') x=expression # unaryLTLExp
    | x=expression op=('precedes'|'follows'|'until'|'after') y=expression ('within' lowerbound=VALUE 'to' upperbound=VALUE('days'|'years'))? # binaryLTLExp
    | x=expression op=('or'|'and') y=expression # binaryLogicExp
    | op='not' x=expression # unaryLogicExp
    | quantifier=('Forall'|'Exists') name=VALUE ' in {' VALUE (',' VALUE)* '}:' x=expression # quantifierExp
    | 'The' comp1=('smaller'|'larger') name=VALUE ' in {' VALUE (',' VALUE)* '} the' comp2=('smaller'|'larger') x=scalar # rankedQuantifierExp;

statement
    : x=column ('is'|'are') str=STRENGTH? desc=COLUMN_DESCRIPTOR ('for' length=VALUE ('days'|'years'))? # columnDescription
    | x=scalar 'is' str=STRENGTH? desc=SCALAR_DESCRIPTOR # scalarDescription
    | x=column op=('<'|'>'|'=') y=column # columnToColumnComparison
    | x=column op=('<'|'>'|'=') val=VALUE # columnToScalarComparison
    | x=scalar op=('<'|'>'|'=') val=VALUE # scalarComparison
    | x=scalar op=('<'|'>'|'=') y=scalar # scalarToScalarComparison
    | x=scalar 'is between' lower=VALUE 'and' upper=VALUE # scalarRangeComparison
    | x=column ('is'|'are') ('counter'|'pro')'cyclical' (('ly ' ('lagging'|'leading'))|'to')? y=column? (('by' lag=VALUE)|('within' lowerbound=VALUE 'to' upperbound=VALUE ))? ('day'|'year'|'lag'|'days'|'years'|'lags')? # cyclicalityStatement;

column
    : atom=var # atomicColumn
    | x=column op=('+'|'-'|'/'|'*') y=column # bivariateColumn
    | op=('growthrate'|'cumulated'|'squared'|'absolute'|'volatility') '(' x=column ')' # univariateColumn //TODO: volatility
    | op=('mean'|'standard deviation'|'variance') '(' xs=columns ')' # rowwiseColumn;
columns: column (',' column)+;
scalar
    : op=('first'|'mean'|'standard deviation'|'sum'|'variance'|'daily volatility'|'kurtosis') '(' x=column ')' # univariateScalar
    | op=('max'|'min') '(' x=scalar ',' y=scalar ')' # bivariateScalar
    | x=scalar op=('+'|'-'|'/'|'*') y=scalar # arithmeticScalar
    | x=scalar 'similar' y=scalar # closenessScalar
    | 'CC(' x=column ',' y=column (',' 'lag=' lag=VALUE)? ')' # crosscorrScalar
    | 'AC('x=column ',' 'lag=' lag=VALUE ')' # autocorrScalar;

var locals [int interval=-1]: col=VAR;

VALUE: '<' [a-zA-Z_]+ '>' | '-'? DIGIT+ ([.] DIGIT+)?;
VAR: '_'[a-zA-Z0-9_]+;
COLUMN_DESCRIPTOR
    : 'strict '? 'monotonically ' ('de'|'in')'creasing'
    | 'persistent'
    | 'stationary'
    | 'non-stationary'
    | 'growing'
    | 'falling'
    | ('light'|'fat'|'heavy')'-'?'tailed'
    | 'flat';
SCALAR_DESCRIPTOR
    : 'positive'
    | 'negative'
    | 'close to zero';
STRENGTH: ('strong'|'medium'|'small');

fragment DIGIT : [0-9];
WHITESPACE : (' '|'\r'|'\n'|'\t') -> skip;

// properties als scalar funktionen?
