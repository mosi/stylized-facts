import antlr4.tree.Tree
from antlr4 import *
from gen.antlr.SFGrammarLexer import SFGrammarLexer
from gen.antlr.SFGrammarVisitor import SFGrammarVisitor
from gen.antlr.SFGrammarParser import SFGrammarParser
from time_series_analysis import *
import pandas as pd
from scipy.stats import linregress

OR = "or"
AND = "and"
NOT = "not"

FAT = "fat"
HEAVY = "heavy"
LIGHT = "light"
TAILED = "tailed"

STATIONARY = "stationary"
NON_STATIONARY = "non-stationary"
PERSISTENT = "persistent"

ALWAYS = "Always"
EVENTUALLY = "Eventually"
FINALLY = "Finally"

UNTIL = "until"
PRECEDES = "precedes"
FOLLOWS = "follows"
AFTER = "after"

NEGATIVE = "negative"
CLOSE_TO_ZERO = "close to zero"
POSITIVE = "positive"

SMALL = "small"
MEDIUM = "medium"
STRONG = "strong"

FALLING = "falling"
FLAT = "flat"
GROWING = "growing"
MONOTONICALLY = "monotonically"

# DF_air = get_air_passenger_data()
DF_fun = get_function_data()
DF_demo = get_demographic_data()
df_SF1, df_SF2, df_SF3, df_SF4, df_SF5 = get_finance_data()

# Specify used data frames and columns.
# 1. element: Data frame
# 2. element: Used column names or empty set. If an empty list is given, then all columns are used
data_sources = [
    (DF_fun, {"linear", "u", "scaled", "sin", "negative_sin", "normal", "exponential", "powerlaw", "linear_no_noise"}),
    # (DF_air, {"passengers"}),
    (DF_demo, {"life_expectancy", "CBR", "fertility", "population_growth"}),
    (df_SF1, {}),
    (df_SF2, {}),
    (df_SF3, {}),
    (df_SF4, {}),
    (df_SF5, {}),
]
# File to fetch SFs from
SF_FILE = "SFFlorian.txt"
# Economy variable to use for SFs like "_var is procyclically lagging"
# Not needed for SFs like "_var is procyclically lagging _negative_sin"
ECONOMY_VAR = "3_gdp_real"

closed_variables = dict()


def get_column(name: str) -> pd.Series:
    for df, names in data_sources:
        if len(names) == 0:
            names = list(df.columns)

        if name in names:
            return df[name]

    raise AttributeError(f"Column {name} not found in specified data sources")


class ExpressionsVisitor(SFGrammarVisitor):
    do_log = True

    def visitExpressions(self, ctx) -> pd.DataFrame:
        expressions = [exp for exp in ctx.children if type(exp) != antlr4.tree.Tree.TerminalNodeImpl]
        exp_strings = [self.extract_original_text(exp) for exp in expressions]
        exp_results = []
        for string, exp in zip(exp_strings, expressions):
            self.do_log = True
            print("Checking [" + string + "]")
            exp_results.append(self.visit(exp))
            print("Expression was " + str(exp_results[-1]))
            print()

        return pd.DataFrame(data=list(zip(exp_strings, exp_results)), columns=['SF', 'validity'])

    def visitBracketedExp(self, ctx) -> bool:
        return self.visit(ctx.x)

    # Implements Always, Eventually and Finally. Always means that the condition is always true.
    # Eventually and Finally both mean that the condition is true at some point
    def visitUnaryLTLExp(self, ctx) -> bool:
        kripke = self.build_kripke(ctx, properties=[ctx.x])

        if ctx.op.text == ALWAYS:
            return all(kripke[0])
        elif ctx.op.text in [EVENTUALLY, FINALLY]:
            return any(kripke[0])
        else:
            raise AttributeError(f"<{ctx.op.text}> is not yet implemented.")

    """
    Implements Until, Precedes, Follows and After.
    
    The \texttt{Until} operator has the same semantics as the Until operator commonly used in temporal logic~\shortcite{Maler2004monitoring}: 
    with \texttt{a Until b}, we expect \texttt{b} to become true at some point (and before this point \texttt{a} has to be true).

    Using the expression \texttt{a Precedes b}, we express that \texttt{a} may not occur after \texttt{b}, and if 
    \texttt{b} is encountered at some point, \texttt{a} must have been true before this point.

    \texttt{Follows} is interpreted similarly to the Release operator of temporal logic: \texttt{a Follows b} implies that 
    \texttt{b} has to be true until and including the point where \texttt{a} first becomes true.

    \texttt{a After b} denotes that \texttt{a} may not occur before \texttt{b} and if \texttt{b} is encountered at some point, 
    \texttt{a} will eventually become true afterward.
    """
    def visitBinaryLTLExp(self, ctx) -> bool:
        kripke = self.build_kripke(ctx, properties=[ctx.x, ctx.y])

        lower = self.get_scalar(ctx.lowerbound) if ctx.lowerbound else 0
        upper = self.get_scalar(ctx.upperbound) if ctx.upperbound else 0

        lastIndex = lambda verts, value: len(verts) - 1 - verts[::-1].index(value) if value in verts else -1
        index = lambda verts, value: verts.index(value) if value in verts else -1

        first_x = index(kripke[0], True)
        first_y = index(kripke[1], True)
        last_x = lastIndex(kripke[0], True)
        last_y = lastIndex(kripke[1], True)

        if ctx.op.text == UNTIL:
            if not ctx.lowerbound:
                # x is true right until but not necessarily including when y becomes true
                return first_y == 0 or (first_y > 0 and all(kripke[0][:first_y]))
            else:
                # x is true at least until start of interval before y
                return first_y == 0 or (first_y > 0 and all(kripke[0][:max(1, first_y-upper)]))
        elif ctx.op.text == PRECEDES:
            if not ctx.lowerbound:
                # y does not happen or x happens and y happens some time after last x
                return first_y == -1 or (0 <= last_x <= first_y)
            else:
                # y does not happen or x happens and y happens within interval after last x
                return first_y == -1 or (0 <= last_x and last_x+lower <= first_y <= last_x+upper)
        elif ctx.op.text == FOLLOWS:
            if not ctx.lowerbound:
                # y is true until and including when x is first true
                return (first_x == -1 and all(kripke[1])) or all(kripke[1][:first_x+1])
            else:
                # y is true at least until start of interval before x
                return (first_x == -1 and all(kripke[1])) or all(kripke[1][:max(1, first_x + 1 - upper)])
        elif ctx.op.text == AFTER:
            if not ctx.lowerbound:
                # first x does not happen before first y
                return first_y <= first_x
            if not ctx.lowerbound:
                # first x happens within interval after first y
                return first_y + lower <= first_x <= first_y + upper
        else:
            raise AttributeError(f"<{ctx.op.text}> is not yet implemented.")

    def visitUnaryLogicExp(self, ctx):
        x = self.visit(ctx.x)
        if ctx.op.text == NOT:
            return not x
        else:
            raise AttributeError(f"<{ctx.op.text}> is not yet implemented.")

    def visitBinaryLogicExp(self, ctx) -> bool:
        x = self.visit(ctx.x)
        y = self.visit(ctx.y)
        if ctx.op.text == OR:
            return x or y
        if ctx.op.text == AND:
            return x and y
        else:
            raise AttributeError(f"<{ctx.op.text}> is not yet implemented.")

    def visitQuantifierExp(self, ctx: SFGrammarParser.QuantifierExpContext) -> bool:
        exp_results = self.check_parameter_assignments(ctx)

        if ctx.quantifier.text == "Forall":
            return all(exp_results)
        elif ctx.quantifier.text == "Exists":
            return any(exp_results)
        else:
            raise AttributeError(f"Quantifier <{ctx.quantifier.text}> is not supported.")

    def visitRankedQuantifierExp(self, ctx) -> bool:
        run_validities = self.check_parameter_assignments(ctx)

        invert = ctx.comp1.text != ctx.comp2.text
        return run_validities == sorted(run_validities, reverse=invert)

    def visitColumnDescription(self, ctx) -> bool:
        column = self.visit(ctx.x)
        if ctx.length:
            column = column.iloc[:self.get_scalar(ctx.length)]

        time_points = [i for i in range(len(column))]

        descriptor = ctx.desc.text
        if descriptor in [GROWING, FALLING, FLAT]:
            slope, intercept, r_value, p_value, std_err = linregress(time_points[:50], column.iloc[:50])

            if p_value > 0.5:
                return False
            if descriptor == GROWING and slope > 0.2:
                return True
            if descriptor == FALLING and slope < -0.2:
                return True
            if descriptor == FLAT and -0.2 <= slope <= 0.2:
                return True
            return False
        elif descriptor == NON_STATIONARY:
            return has_unit_root(column)
        elif descriptor == STATIONARY:
            return not has_unit_root(column)
        elif descriptor == PERSISTENT:
            return auto_correlation(column, 2) > 0.5
        elif TAILED in descriptor:
            kurtosis = right_side_kurtosis(column, 10000)
            if self.do_log:
                print(f"Right side kurtosis of time series is {kurtosis}")
            if LIGHT in descriptor and kurtosis <= -1:
                return True
            elif FAT in descriptor or HEAVY in descriptor:
                if kurtosis >= 1:
                    return True
            return False
        elif MONOTONICALLY in descriptor:
            if "strict" in descriptor and not column.is_unique:
                return False
            if "increasing" in descriptor:
                return column.is_monotonic_increasing
            else:
                return column.is_monotonic_decreasing
        else:
            raise AttributeError(f"<{ctx.op.text}> is not yet implemented.")

    def visitScalarDescription(self, ctx) -> bool:
        scalar = self.visit(ctx.x)

        comparison_value = 0.5
        if ctx.str_:
            if ctx.str_.text == SMALL:
                comparison_value = 0.2
            if ctx.str_.text == MEDIUM:
                comparison_value = 0.5
            if ctx.str_.text == STRONG:
                comparison_value = 0.8

        descriptor = ctx.desc.text

        if descriptor == POSITIVE:
            return scalar > comparison_value
        elif descriptor == NEGATIVE:
            return scalar < -comparison_value
        elif descriptor == CLOSE_TO_ZERO:
            return -1 + comparison_value < scalar < 1 - comparison_value
        else:
            raise AttributeError(f"<{ctx.op.text}> is not yet implemented.")

    def visitScalarComparison(self, ctx) -> bool:
        x = self.visit(ctx.x)
        val = self.get_scalar(ctx.val)
        op = ctx.op.text
        return self.compare(x, val, op)

    def visitScalarToScalarComparison(self, ctx) -> bool:
        x = self.visit(ctx.x)
        y = self.visit(ctx.y)
        op = ctx.op.text
        return self.compare(x, y, op)

    def visitColumnToColumnComparison(self, ctx) -> bool:
        xs = self.visit(ctx.x)
        ys = self.visit(ctx.y)
        sum1 = sum(xs)
        sum2 = sum(ys)
        op = ctx.op.text
        return self.compare(sum1, sum2, op)

    def visitColumnToScalarComparison(self, ctx) -> bool:
        xs = self.visit(ctx.x)
        val = self.get_scalar(ctx.val)
        op = ctx.op.text
        return self.compare(xs.iat[0], val, op)

    def visitScalarRangeComparison(self, ctx) -> bool:
        x = self.visit(ctx.x)
        lower = self.get_scalar(ctx.lower)
        upper = self.get_scalar(ctx.upper)
        return lower <= x <= upper

    def visitCyclicalityStatement(self, ctx) -> float:
        x = self.visit(ctx.x)
        if ctx.y:
            y = self.visit(ctx.y)
        else:
            y = get_column(ECONOMY_VAR)

        exp = ctx.getText()

        if ctx.lag:
            lower = self.get_scalar(ctx.lag)
            upper = self.get_scalar(ctx.lag)
        elif ctx.lowerbound:
            lower = self.get_scalar(ctx.lowerbound)
            upper = self.get_scalar(ctx.upperbound)
        elif "leading" in exp or "lagging" in exp:
            lower = 5
            upper = 10
            if self.do_log:
                print(f"Use default lags between {lower} and {upper}.")
        else:
            lower = 0
            upper = 0

        if "leading" in exp:
            lower = -lower
            upper = -upper

        lower, upper = min(lower, upper), max(lower, upper)

        if self.do_log:
            print(f"Test cross correlation with all lags between {lower} and {upper}.")
        for lag in range(lower, upper + 1):
            correlation = cross_correlation(x, y, lag)
            if self.do_log:
                print(f"{lag}: {correlation}")
            if "procyclical" in exp:
                if correlation > 0.5:
                    return True
            elif correlation < -0.5:
                return True
        return False

    def visitAtomicColumn(self, ctx) -> pd.Series:
        time_interval = ctx.atom.interval
        var = ctx.atom.children[0]
        name = var.symbol.text[1:]
        column = get_column(name)

        if time_interval == -1:
            return column
        else:
            return column[time_interval[0]:time_interval[1] + 1]

    def visitBivariateColumn(self, ctx) -> pd.Series:
        x = self.visit(ctx.x)
        y = self.visit(ctx.y)

        if ctx.op.text == '+':
            return x + y
        elif ctx.op.text == '-':
            return x - y
        if ctx.op.text == '*':
            return x * y
        elif ctx.op.text == '/':
            return x / y
        else:
            raise AttributeError(f"<{ctx.op.text}> is not yet implemented.")

    def visitUnivariateColumn(self, ctx) -> pd.Series:
        x = self.visit(ctx.x)
        if ctx.op.text == 'cumulated':
            return cumulated(x)
        elif ctx.op.text == 'absolute':
            return absolute(x)
        elif ctx.op.text == 'squared':
            return squared(x)
        elif ctx.op.text == 'growthrate':
            return growth_rate(x)
        else:
            raise AttributeError(f"<{ctx.op.text}> is not yet implemented.")

    def visitRowwiseColumn(self, ctx) -> pd.Series:
        df = self.visit(ctx.xs)

        if ctx.op.text == 'mean':
            return df.mean(axis=1)
        if ctx.op.text == 'standard deviation':
            return df.std(axis=1)
        if ctx.op.text == 'variance':
            return df.var(axis=1)
        else:
            raise AttributeError(f"<{ctx.op.text}> is not yet implemented.")

    def visitColumns(self, ctx) -> pd.DataFrame:
        columns = [self.visit(col) for col in ctx.children if type(col) != antlr4.tree.Tree.TerminalNodeImpl]
        return pd.concat(columns, axis=1)

    def visitUnivariateScalar(self, ctx) -> float:
        x = self.visit(ctx.x)
        if ctx.op.text == 'first':
            return x.iat[0]
        if ctx.op.text == 'mean':
            return x.mean()
        if ctx.op.text == 'standard deviation':
            return x.std()
        if ctx.op.text == 'variance':
            return x.var()
        if ctx.op.text == 'daily volatility':
            return daily_volatility(x)
        if ctx.op.text == 'kurtosis':
            return right_side_kurtosis(x)
        elif ctx.op.text == 'sum':
            return sum(x)
        else:
            raise AttributeError(f"<{ctx.op.text}> is not yet implemented.")

    def visitBivariateScalar(self, ctx) -> float:
        x = self.visit(ctx.x)
        y = self.visit(ctx.y)

        if ctx.op.text == 'max':
            return max(x, y)
        if ctx.op.text == 'min':
            return min(x, y)
        else:
            raise AttributeError(f"<{ctx.op.text}> is not yet implemented.")

    def visitArithmeticScalar(self, ctx) -> float:
        x = self.visit(ctx.x)
        y = self.visit(ctx.y)

        if ctx.op.text == '+':
            return x + y
        if ctx.op.text == '-':
            return x - y
        if ctx.op.text == '*':
            return x * y
        elif ctx.op.text == '/':
            return x / y
        else:
            raise AttributeError(f"<{ctx.op.text}> is not yet implemented.")

    def visitCrosscorrScalar(self, ctx) -> float:
        x = self.visit(ctx.x)
        y = self.visit(ctx.y)
        lag = 0
        if ctx.lag:
            lag = self.get_scalar(ctx.lag)

        CC = cross_correlation(x, shift(y, lag))
        print(f"Cross correlation was: {CC}")

        return CC

    def visitAutocorrScalar(self, ctx) -> float:
        x = self.visit(ctx.x)
        lag = self.get_scalar(ctx.lag)
        return auto_correlation(x, lag)

    def build_kripke(self, ctx, properties: list) -> list:
        do_log = self.do_log
        self.do_log = False

        column_vars = self.fetch_vars(ctx)

        inner_ltls = self.fetch_inner_ltls(ctx)
        inner_ltls.remove(ctx)

        # if intervals set from outer ltl, use these as start and end time
        if ctx.interval != -1:
            start_time = ctx.interval[0]
            end_time = ctx.interval[1]
        # if no intervals set, use max length of column variables
        elif len(column_vars) > 0:
            start_time = 0
            end_time = max([len(get_column(col.children[0].symbol.text[1:])) for col in column_vars])
        else:
            start_time = 0
            end_time = 500

        skip_last = 20
        tested_max_time = end_time - skip_last


        held_properties = [[] for _ in properties]
        for time_point in range(start_time, tested_max_time + 1):
            for var in column_vars:
                var.interval = (time_point, end_time)
            for ltl in inner_ltls:
                ltl.interval = (time_point, end_time)

            for i, cur_property in enumerate(properties):
                held_properties[i].append(self.visit(cur_property))

        if do_log:
            for j in range(len(properties)):
                print("Property", j, ":", end="")
                for time_point in range(tested_max_time + 1):
                    print("#" if held_properties[j][time_point] else "_", end="")
                print()

        return held_properties

    def check_parameter_assignments(self, ctx) -> list:
        if not ctx.name.text.startswith("<"):
            raise AttributeError(f"Bound variable {ctx.name.text} has to be surrounded by angle brackets '<>'.")

        assignments = []
        for child in ctx.children:
            if type(child) == antlr4.tree.Tree.TerminalNodeImpl:
                try:
                    assignments.append(int(child.symbol.text))
                except ValueError:
                    pass
        assignments.sort()

        exp_validities = []
        for assignment in assignments:
            closed_variables[ctx.name.text] = assignment
            cur_validity = self.visit(ctx.x)
            if self.do_log:
                print(f"{ctx.name.text}={assignment} resulted in {cur_validity}")
            exp_validities.append(cur_validity)
        return exp_validities

    def compare(self, x, y, op) -> bool:
        if self.do_log:
            print(f"Checking {x} {op} {y}.")
        if op == "<":
            return x < y
        elif op == ">":
            return x > y
        elif op == "=":
            return x == y
        else:
            raise AttributeError(f"Comparison <{op}> is not yet implemented.")

    @staticmethod
    def get_scalar(x):
        if not type(x) == str:
            x = x.text
        if x.startswith("<"):
            return closed_variables[x]
        if "." in x:
            return float(x)
        else:
            return int(x)

    @staticmethod
    def extract_original_text(ctx) -> str:
        token_source = ctx.start.getTokenSource()
        input_stream = token_source.inputStream
        start, stop = ctx.start.start, ctx.stop.stop
        return input_stream.getText(start, stop)

    @staticmethod
    def fetch_vars(ctx):
        return ExpressionsVisitor.fetch_nodes_of_type(ctx, SFGrammarParser.VarContext)

    @staticmethod
    def fetch_inner_ltls(ctx):
        return ExpressionsVisitor.fetch_nodes_of_type(ctx, SFGrammarParser.UnaryLTLExpContext) + \
            ExpressionsVisitor.fetch_nodes_of_type(ctx, SFGrammarParser.BinaryLTLExpContext)

    @staticmethod
    def fetch_nodes_of_type(ctx, type):
        if isinstance(ctx, antlr4.TerminalNode):
            return []

        nodes = []
        if isinstance(ctx, type):
            nodes.append(ctx)
        for child in ctx.children:
            nodes = nodes + ExpressionsVisitor.fetch_nodes_of_type(child, type)
        return nodes


if __name__ == '__main__':
    with open(SF_FILE, "r") as f:
        stream = InputStream(f.read())
        lexer = SFGrammarLexer(stream)
        stream = CommonTokenStream(lexer)
        parser = SFGrammarParser(stream)
        root = parser.expressions()
        visitor = ExpressionsVisitor()
        sf_validity_df = visitor.visitExpressions(root)

        print()
        pd.set_option('display.max_columns', 100)
        print(sf_validity_df)
