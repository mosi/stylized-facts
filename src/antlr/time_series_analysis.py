import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from statsmodels.tsa.stattools import adfuller
from statsmodels.tsa.filters.bk_filter import bkfilter
from scipy.stats import kurtosis
from random import choices


def get_finance_data():
    df_SF1 = pd.read_csv("../recources/SF1.csv")
    df_SF1 = pd.DataFrame(df_SF1, columns=["gdp_real", "real_investment", "consumption", "time"])
    df_SF1 = df_SF1.set_index("time")
    df_SF1 = df_SF1.rename(columns={
        "gdp_real": "1_gdp_real",
        "real_investment": "1_real_investment",
        "consumption": "1_consumption",
    })

    df_SF2 = pd.read_csv("../recources/SF2.csv")
    df_SF2 = pd.DataFrame(df_SF2, columns=["y"])
    df_SF2 = df_SF2.rename(columns={"y": "2_recession_period_length"})

    df_SF3 = pd.read_csv("../recources/SF3.csv")
    df_SF3 = pd.DataFrame(df_SF3, columns=["gdp_real", "stock_change", "inflation", "price", "unemployees", "time"])
    df_SF3 = df_SF3.set_index("time")
    df_SF3 = df_SF3.rename(
        columns={"gdp_real": "3_gdp_real", "stock_change": "3_stock_change", "inflation": "3_inflation",
                 "price": "3_price", "unemployees": "3_unemployees"})

    df_SF4 = pd.read_csv("../recources/SF4.csv")
    df_SF4 = pd.DataFrame(df_SF4, columns=["gdp_real", "investment", "time"])
    df_SF4 = df_SF4.set_index("time")
    df_SF4 = df_SF4.rename(columns={"gdp_real": "4_gdp_real", "investment": "4_investment"})

    df_SF5 = pd.read_csv("../recources/SF5.csv")
    df_SF5 = pd.DataFrame(df_SF5, columns=["gdp_real", "firmDebt", "bankProfits", "default", "time"])
    df_SF5 = df_SF5.set_index("time")
    df_SF5 = df_SF5.rename(
        columns={"gdp_real": "5_gdp_real", "firmDebt": "5_firm_debt", "bankProfits": "5_bank_profits",
                 "default": "5_credit_default"})

    df_SF1 = apply_log_and_baxter_king(df_SF1)
    df_SF3 = apply_log_and_baxter_king(df_SF3)
    df_SF4 = apply_log_and_baxter_king(df_SF4)
    df_SF5 = apply_log_and_baxter_king(df_SF5)

    # remove warmup
    df_SF3 = df_SF3[200:]
    df_SF5 = df_SF5[200:]

    df_SF3["3_inflation"] *= 100
    return [df_SF1, df_SF2, df_SF3, df_SF4, df_SF5]


def plot_finance_data() -> None:
    dfs = get_finance_data()
    for df in dfs:
        df.rename(columns=(lambda name: name[2:] if type(name) == str else name), inplace=True)
        df.rename(columns=(lambda name: name.replace("_", " ") if type(name) == str else name), inplace=True)

    dfs[0].rename(columns={
        "gdp real": "real gdp [production units]",
        "real investment": "real investment [production units]",
        "consumption": "consumption [production units]",
    }, inplace=True)

    dfs[2].rename(columns={
        "gdp real": "real gdp [production units]",
        "inflation": "inflation [percent*100]",
        "stock change": "stock change [production units]",
        "price": "price [nominal units]",
        "unemployees": "unemployment [percent]",
    }, inplace=True)

    dfs[3].rename(columns={
        "gdp real": "real gdp [production units]",
        "investment": "investment [production units]",
    }, inplace=True)

    dfs[4].rename(columns={
        "gdp real": "real gdp [production units]",
        "firm debt": "firm debt [nominal units]",
        "bank profits": "bank profits [nominal units]",
        "credit default": "credit default [nominal units]",
    }, inplace=True)

    # titles = ["SF1", "SF2", "SF3", "SF4", "SF5"]
    xlabels = [
        "time in periods",
        "recession",
        "time in periods",
        "time in periods",
        "time in periods",
    ]
    ylabels = [
        "",
        "duration of recessions in periods",
        "",
        "",
        "",
    ]

    linestyles = ["-", "--", ":", "-.", "-"]

    prevParams = plt.rcParams
    for i in [1]:

        plt.figure()
        dfs[i].plot()
        plt.legend(loc="best")
        plt.xlabel(xlabels[i])
        plt.ylabel(ylabels[i])

        plt.savefig("../../paper/figures/plot_SF" + str(i + 1) + ".pdf")

        plt.rc("font", size=8)
        plt.rc("axes", titlesize=8)
        plt.rc("xtick", labelsize=8)  # fontsize of the tick labels
        plt.rc("ytick", labelsize=8)  # fontsize of the tick labels
        plt.rc("legend", fontsize=8)  # legend fontsize
        plt.rc("figure", titlesize=8)  # fontsize of the figure title
        plt.rcParams["figure.figsize"] = (6, 3.2)
        plt.rcParams["axes.linewidth"] = 0.5
        plt.rcParams["lines.linewidth"] = 1
        plt.figure()
        dfs[i].plot()
        plt.legend(loc="best")
        plt.xlabel(xlabels[i])
        plt.ylabel(ylabels[i])

        plt.savefig("../../paper/figures/plot_SF" + str(i + 1) + "_long.pdf")
        plt.rcParams = prevParams

    for i in [0, 2, 3, 4]:


        plt.figure()
        dfs[i] = dfs[i].loc[250:350]
        dfs[i].plot(style=linestyles)
        if i in [2, 3]:
            plt.minorticks_on()
            # plt.xticks(np.arange(250, 351, 1.0))
        plt.legend(loc="best")
        plt.xlabel(xlabels[i])
        plt.ylabel(ylabels[i])
        plt.savefig("../../paper/figures/plot_SF" + str(i + 1) + ".pdf")


def apply_log_and_baxter_king(df: pd.DataFrame) -> pd.DataFrame:
    for col in df.columns:
        if "inflation" not in col and "stock_change" not in col:
            df[col] = np.log(df[col])
    df = df.replace(-np.inf, 0)
    df = bkfilter(df)

    df.fillna(0)
    df.rename(columns=(lambda name: name[:-6] if type(name) == str else name), inplace=True)

    return df


# def get_air_passenger_data() -> pd.DataFrame:
#     df = pd.read_csv("../recources/AirPassengers.csv")
#     df["Month"] = pd.to_datetime(df["Month"], format="%Y-%m")
#     df.index = df["Month"]
#     del df["Month"]
#     df.rename(columns={"#Passengers": "passengers"}, inplace=True)
#
#     return df


def get_function_data(length=500) -> pd.DataFrame:
    shrink = 5
    index = [i for i in range(length)]
    linear = [i / shrink for i in index]
    df = pd.DataFrame(linear, index=index, columns=["linear"])

    sin = lambda series: np.sin(series) * 20

    def u(series):
        return (series - length / shrink / 2) ** 2 / 5

    def scale(series):
        return 5 * series

    def std_normal(x):
        prob_density = (1 / np.sqrt(2 * np.pi)) * np.exp(-0.5 * x ** 2)
        return prob_density * 500

    def exponential(x, rate):
        prob_density = rate * np.exp(-rate * x)
        return prob_density * 500

    def powerlaw(x, k):
        if x == 0:
            return 0
        else:
            return x ** -k

    df["u"] = df.apply(lambda x: u(x["linear"]), axis=1)
    df["scaled"] = df.apply(lambda x: scale(x["linear"]), axis=1)
    df["sin"] = df.apply(lambda x: sin(x["linear"]), axis=1)
    df["negative_sin"] = df.apply(lambda x: -sin(x["linear"]), axis=1)
    df["normal"] = df.apply(lambda x: std_normal(x["linear"]), axis=1)
    df["exponential"] = df.apply(lambda x: exponential(x["linear"], 1), axis=1)
    df["powerlaw"] = df.apply(lambda x: powerlaw(x["linear"], 2), axis=1)
    df["linear_no_noise"] = df.apply(lambda x: x["linear"], axis=1)

    noise = np.random.normal(0, 2, [length, 5])
    noise = np.c_[noise, np.zeros([length, 4])]

    return df + noise


def get_demographic_data(length=500) -> pd.DataFrame:
    life_expectancy = lambda x: x / 4
    population_growth = lambda x: 5 + x / 4 if x <= 300 else (300 - x) / 4 + 80

    def fertility(x):
        if x <= 100:
            return x / 2 + 50
        elif x <= 200:
            return -x / 2 + 150
        elif x <= 300:
            return 50
        else:
            return -x / 2 + 200

    CBR = lambda x: 1 - x / 500

    index = [i for i in range(length)]
    df = pd.DataFrame(index, index=index, columns=["index"])
    df["life_expectancy"] = df["index"].apply(life_expectancy)
    df["population_growth"] = df["index"].apply(population_growth)
    df["fertility"] = df["index"].apply(fertility)
    df["CBR"] = df["index"].apply(CBR)
    df = df.drop("index", axis=1)

    return df


def plot_df(df: pd.DataFrame, title: str = "", xlabel: str = "Year", ylabel: str = "#Passengers") -> None:
    plt.figure(figsize=(16, 5), dpi=200)
    plt.plot(df["Month"], df["#Passengers"], color="tab:red")
    plt.gca().set(title=title, xlabel=xlabel, ylabel=ylabel)
    plt.show()


def cumulated(series: pd.Series) -> pd.Series:
    return series.cumsum()


def absolute(series: pd.Series) -> pd.Series:
    return series.abs()


def squared(series: pd.Series) -> pd.Series:
    return series ** 2


def growth_rate(col: pd.Series) -> pd.Series:
    gr = col.pct_change().fillna(0.0)
    return gr


def shift(col: pd.Series, lag: int) -> pd.Series:
    return col.shift(lag)


def auto_correlation(col: pd.Series, lag: int) -> float:
    return col.autocorr(lag=lag)


def cross_correlation(column_x: pd.Series, column_y: pd.Series, lag=0) -> float:
    return column_x.corr(shift(column_y, lag))


def daily_volatility(returns: pd.Series) -> float:
    log_returns = np.log(returns / returns.shift())
    return log_returns.std()


def has_unit_root(df: pd.DataFrame) -> bool:
    threshhold = 0.05
    p_value = adfuller(df)[1]

    print("Pvalue of adfuller test:", p_value)
    # Fail to reject the null hypothesis (H0), the data has a unit root and is non-stationary
    has_unit_root = p_value > threshhold
    return has_unit_root


def right_side_kurtosis(time_series: pd.Series, n_draws=10000) -> float:
    symmetric_series = make_symmetric_distribution(time_series)
    symmetric_series_np = symmetric_series.to_numpy()
    normal_dist = symmetric_series_np / sum(symmetric_series_np)

    draws = [choices(symmetric_series.index.to_numpy(), normal_dist) for i in range(n_draws)]
    return kurtosis(draws, bias=False)[0]


def make_symmetric_distribution(time_series: pd.Series) -> pd.Series:
    np_series = time_series.to_numpy()
    without_zero = np_series[1:]
    negatives = np.flip(without_zero)

    distribution = np.append(negatives, np_series)
    return pd.Series(distribution)


if __name__ == "__main__":
    plot_finance_data()
